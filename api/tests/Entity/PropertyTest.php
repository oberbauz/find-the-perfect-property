<?php

namespace App\Tests\Entity;


use App\Entity\Property;
use PHPUnit\Framework\TestCase;

class PropertyTest extends TestCase
{
    public function testCityExtractionFromAddress()
    {
        $city = 'München';
        $propertyAddressWithoutTheCity = 'Ledererstraße 8, ';

        $property = new Property();
        $property->setAddress($propertyAddressWithoutTheCity.$city);

        $this->assertEquals($city, $property->getCity());
    }
}