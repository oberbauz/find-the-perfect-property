<?php

namespace App\Tests\Repository;

use App\Entity\Property;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PropertyRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testRetrievalOfPropertyById()
    {
        // Select the smallest ID possible.
        $id = $this->entityManager->createQuery(
            'SELECT MIN(p.id) FROM App:Property p'
        )->getSingleScalarResult();

        $property = $this->entityManager
            ->getRepository(Property::class)
            ->find($id)
        ;

        $this->assertInstanceOf(Property::class, $property);
        $this->assertEquals($id, $property->getId());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}