<?php

namespace App\Tests\Repository;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testRetrievalOfUserById()
    {
        // Select the smallest ID possible.
        $id = $this->entityManager->createQuery(
            'SELECT MIN(u.id) FROM App:User u'
        )->getSingleScalarResult();

        $user = $this->entityManager
            ->getRepository(User::class)
            ->find($id)
        ;

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($id, $user->getId());
    }

    public function testRetrievalOfUserByEmail()
    {
        // Fixtures must be loaded to make this work!
        $email = 'tester@mcnn.de';

        $user = $this->entityManager
            ->getRepository(User::class)
            ->findByMail($email)
        ;

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($email, $user->getEmail());
    }

    public function testRetrievalOfUserByUsername()
    {
        // Fixtures must be loaded to make this work!
        $username = 'tester';

        $user = $this->entityManager
            ->getRepository(User::class)
            ->findUserByUsername($username)
        ;

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($username, $user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}