<?php

namespace App\Tests\Repository;

use App\Entity\BookingRequest;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookingRequestRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testRetrievalOfBookingRequestById()
    {
        // Select the smallest ID possible.
        $id = $this->entityManager->createQuery(
            'SELECT MIN(br.id) FROM App:BookingRequest br'
            )->getSingleScalarResult();

        $bookingRequest = $this->entityManager
            ->getRepository(BookingRequest::class)
            ->find($id)
        ;

        $this->assertInstanceOf(BookingRequest::class, $bookingRequest);
        $this->assertEquals($id, $bookingRequest->getId());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}