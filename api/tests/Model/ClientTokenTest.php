<?php

namespace App\Tests\Model;

use App\Model\ClientToken;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;

class ClientTokenTest extends TestCase
{
    public function testCreationWithFakeRequest()
    {
        $clientTokenString = '88ff8fc9-73ff-4b5e-b195-7a8acb139552';

        $request = $this->createMock(Request::class);
        $headerBag = $this->createMock(HeaderBag::class);

        $headerBag
            ->expects($this->once())
            ->method('get')
            ->with('x-token', null, true)
            ->willReturn($clientTokenString);

        $request->headers = $headerBag;

        $clientToken = ClientToken::createFromRequest($request);

        $this->assertInstanceOf(ClientToken::class, $clientToken);
        $this->assertEquals($clientTokenString, $clientToken->getToken());
    }
    
    public function testExceptionWithMissingInformation()
    {
        $request = $this->createMock(Request::class);

        $headerBag = $this->createMock(HeaderBag::class);

        $headerBag
            ->expects($this->once())
            ->method('get')
            ->with('x-token', null, true)
            ->willReturn(null);

        $request->headers = $headerBag;

        // The ClientToken object can only be created if the token header is set.
        $this->expectException(\InvalidArgumentException::class);
        ClientToken::createFromRequest($request);
    }
}