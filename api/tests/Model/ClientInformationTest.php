<?php

namespace App\Tests\Model;

use App\Model\ClientInformation;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;

class ClientInformationTest extends TestCase
{
    public function testCreationWithFakeRequest()
    {
        $ipAddress = '127.0.0.1';
        $useragent = 'Opera/9.80 (X11; Linux i686; U; ru) Presto/2.8.131 Version/11.11';
        $clientId = '378trgof387g2o87dgo837g';

        $request = $this->createMock(Request::class);

        $request
            ->expects($this->once())
            ->method('getClientIp')
            ->will($this->returnValue($ipAddress))
        ;

        $headerBag = $this->createMock(HeaderBag::class);

        $headerBag
            ->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(['user-agent', null, true], ['x-client-id', null, true])
            ->willReturnOnConsecutiveCalls($useragent, $clientId)
        ;

        $request->headers = $headerBag;

        $clientInformation = ClientInformation::createFromRequest($request);

        $this->assertInstanceOf(ClientInformation::class, $clientInformation);
        $this->assertEquals($clientInformation->flatten(), $ipAddress . $useragent . $clientId);
    }
    
    public function testExceptionWithMissingInformation()
    {
        $request = $this->createMock(Request::class);

        $request
            ->expects($this->once())
            ->method('getClientIp')
            ->will($this->returnValue('127.0.0.1'))
        ;

        $headerBag = $this->createMock(HeaderBag::class);

        $headerBag
            ->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(['user-agent', null, true], ['x-client-id', null, true])
            ->willReturnOnConsecutiveCalls('Opera/9.80 (X11; Linux i686; U; ru) Presto/2.8.131 Version/11.11', null)
        ;

        $request->headers = $headerBag;

        // The ClientInformation object can only be created if all information is set.
        $this->expectException(\InvalidArgumentException::class);
        ClientInformation::createFromRequest($request);
    }
}