<?php

namespace App\Tests\Model;

use App\Model\UserToken;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;

class UserTokenTest extends TestCase
{
    public function testCreationWithFakeRequest()
    {
        $creationDate = new \DateTimeImmutable();
        $tokenString = '88ff8fc9-73ff-4b5e-b195-7a8acb139552';

        $userToken = new UserToken(
            $creationDate,
            $tokenString
        );

        $this->assertEquals($creationDate, $userToken->getCreationDate());
        $this->assertEquals($tokenString, $userToken->getToken());
    }
}