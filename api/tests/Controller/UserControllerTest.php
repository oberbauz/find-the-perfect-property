<?php

namespace App\Tests\Controller;


use Symfony\Bridge\Doctrine\DataCollector\DoctrineDataCollector;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;

class UserControllerTest extends WebTestCase
{
    public function testRegisterUser()
    {
        $client = static::createClient([], [
            'HTTP_HOST'        => 'en.example.com',
            'HTTP_USER_AGENT'  => 'PhpTest/1.0',
            'HTTP_X_CLIENT_ID' => '88ff8fc9-73ff-4b5e-b195-7a8acb139552',
        ]);

        // Enable the profiler to collect all mails sent. If the profiler isn't found nothing happens.
        $client->enableProfiler();

        $client->request('POST', '/users/register', [], [], [], \GuzzleHttp\json_encode([
            'email' => 'testemail@mcnn.de',
            'username' => 'usernameThatIsUnique'
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // check that the profiler is enabled
        if ($profile = $client->getProfile()) {
            /** @var DoctrineDataCollector $doctrineDataCollector */
            $doctrineDataCollector = $profile->getCollector('db');
            $queries = $doctrineDataCollector->getQueries()['default'];

            $insertQuery = false;
            foreach ($queries as $query) {
                if(
                    strpos($query['sql'], 'INSERT INTO user') !== false
                    || strpos($query['sql'],'UPDATE user') !== false
                ) {
                    $insertQuery = true;
                }
            }

            $this->assertTrue(
                $insertQuery,
                'A query inserting or updating the user in the database should be executed on register.'
            );

            /** @var MessageDataCollector $messageDataCollector */
            $messageDataCollector = $profile->getCollector('swiftmailer');
            $this->assertEquals(
                1,
                $messageDataCollector->getMessageCount(),
                'Exactly one message should be sent to the user!'
            );
        }
    }

    public function testBookingOfAProperty()
    {
        $clientHeaders = [
            'HTTP_HOST'        => 'en.example.com',
            'HTTP_USER_AGENT'  => 'PhpTest/1.0',
            'HTTP_X_CLIENT_ID' => '88ff8fc9-73ff-4b5e-b195-7a8acb139552',
        ];

        $client = static::createClient([], $clientHeaders);

        $email = 'testemail@mcnn.de';
        $username = 'usernameThatIsUnique';

        // This placeId should be present in the database!
        $propertyPlaceId = 'ChIJc2TJ9P2ipBIRA65Mte-Mak8';

        // Enable the profiler to collect all mails sent. If the profiler isn't found nothing happens.
        $client->enableProfiler();

        $client->request('POST', '/users/register', [], [], [], \GuzzleHttp\json_encode([
            'email' => $email,
            'username' => $username
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // check that the profiler is enabled
        if ($profile = $client->getProfile()) {
            /** @var MessageDataCollector $messageDataCollector */
            $messageDataCollector = $profile->getCollector('swiftmailer');
            $this->assertEquals(
                1,
                $messageDataCollector->getMessageCount(),
                'Exactly one message should be sent to the user!'
            );

            $mailSent = $messageDataCollector->getMessages()[0];
            $tokenStart = strpos($mailSent->getBody(), '?token=') + 7;

            // Extract the sent token for authentication.
            $token = substr($mailSent->getBody(), $tokenStart, 32);

            $clientHeaders['HTTP_X_TOKEN'] = $token;
            $client->request('POST', '/users/'.$username.'/book/'.$propertyPlaceId, [], [], $clientHeaders);

            $this->assertEquals(200, $client->getResponse()->getStatusCode());
        }
    }

    public function testUserAuthenticationWhenBookingAProperty()
    {
        $clientHeaders = [
            'HTTP_HOST'        => 'en.example.com',
            'HTTP_USER_AGENT'  => 'PhpTest/1.0',
            'HTTP_X_CLIENT_ID' => '88ff8fc9-73ff-4b5e-b195-7a8acb139552',
        ];

        $client = static::createClient([], $clientHeaders);

        $email = 'testemail@mcnn.de';
        $username = 'usernameThatIsUnique';

        // This placeId should be present in the database!
        $propertyPlaceId = 'ChIJc2TJ9P2ipBIRA65Mte-Mak8';

        // Enable the profiler to collect all mails sent. If the profiler isn't found nothing happens.
        $client->enableProfiler();

        $client->request('POST', '/users/register', [], [], [], \GuzzleHttp\json_encode([
            'email' => $email,
            'username' => $username
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // check that the profiler is enabled
        if ($profile = $client->getProfile()) {
            /** @var MessageDataCollector $messageDataCollector */
            $messageDataCollector = $profile->getCollector('swiftmailer');

            // Check if Mail with login information was sent.
            $this->assertEquals(
                1,
                $messageDataCollector->getMessageCount(),
                'Exactly one message should be sent to the user!'
            );

            $mailSent = $messageDataCollector->getMessages()[0];
            $tokenStart = strpos($mailSent->getBody(), '?token=') + 7;

            // Extract the sent token for authentication.
            $token = substr($mailSent->getBody(), $tokenStart, 32);

            $clientHeaders['HTTP_X_TOKEN'] = $token;
            $client->request('POST', '/users/'.$username.'alternate/book/'.$propertyPlaceId, [], [], $clientHeaders);

            $this->assertEquals(
                403,
                $client->getResponse()->getStatusCode(),
                'Booking a property for another user should be forbidden!'
            );
        }
    }
}