<?php

namespace App\Tests\Util\Validator;


use App\Exception\InvalidEmailAddress;
use App\Util\Validator\EmailValidator;
use PHPUnit\Framework\TestCase;

class EmailValidatorTest extends TestCase
{
    /**
     * @dataProvider validEmailAddressProvider
     */
    public function testEmailValidationWithValidEmailAddresses(string $value)
    {
        $this->assertTrue(EmailValidator::validate($value));
    }

    public function validEmailAddressProvider()
    {
        return [
            ['valid@mail.com'],
            ['some.test.mail@gmail.com'],
            ['some.test.mail+123@googlemail.com'],
            ['tester@mcnn.de'],
            ['testing@mcnn.de']
        ];
    }

    /**
     * @dataProvider invalidEmailAddressProvider
     */
    public function testEmailValidationWithInvalidEmailAddresses(string $value)
    {
        $this->expectException(InvalidEmailAddress::class);
        EmailValidator::validate($value);
    }

    public function invalidEmailAddressProvider()
    {
        return [
            ['invalidmail'],
            ['invalidm@il'],
            ['@invalidmail'],
            ['inv alid@mail.com'],
            ['invalid@mai+l.com'],
            ['inv@lid@mail.com'],
            ['']
        ];
    }
}