<?php

namespace App\Tests\Util\Validator;


use App\Exception\InvalidUsernameException;
use App\Util\Validator\UsernameValidator;
use PHPUnit\Framework\TestCase;

class UsernameValidatorTest extends TestCase
{
    /**
     * @dataProvider validUsernameProvider
     */
    public function testUsernameValidationWithValidUsername(string $value)
    {
        $this->assertTrue(UsernameValidator::validate($value));
    }

    public function validUsernameProvider()
    {
        return [
            ['test'],
            ['ali213'],
            ['bauz'],
            ['mathias84'],
            ['Erica23'],
            ['24manta292'],
        ];
    }

    /**
     * @dataProvider invalidUsernameProvider
     */
    public function testUsernameValidationWithInvalidUsername(string $value)
    {
        $this->expectException(InvalidUsernameException::class);
        UsernameValidator::validate($value);
    }

    public function invalidUsernameProvider()
    {
        return [
            ['invalid username'],
            ['invalidÜser'],
            ['@wrong'],
            ['invalid!!'],
            ['op&dgh'],
            ['kjcb\dgwj'],
            ['']
        ];
    }
}