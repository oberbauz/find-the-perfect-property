<?php


namespace App\Tests\Service;


use App\Entity\User;
use App\Model\ClientInformation;
use App\Model\UserToken;
use App\Repository\UserRepository;
use App\Service\TokenService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    public function testRegistrationOfUser()
    {
        $email = 'newregistered@example.com';
        $username = 'uniqueUsername287358';

        $clientInformation = $this->createMock(ClientInformation::class);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $tokenService = $this->createMock(TokenService::class);
        $mailer = $this->createMock(\Swift_Mailer::class);

        $userRepository = $this->createMock(UserRepository::class);

        $userRepository
            ->method('findByMail')
            ->willReturn(null);
        $userRepository
            ->method('findUserByUsername')
            ->willReturn(null);

        $entityManager
            ->expects($this->exactly(2))
            ->method('getRepository')
            ->withConsecutive([User::class], [User::class])
            ->willReturnOnConsecutiveCalls($userRepository, $userRepository);

        $entityManager
            ->expects($this->once())
            ->method('persist');

        $userToken = $this->createMock(UserToken::class);
        $userTokenCreationDate = new \DateTimeImmutable();
        $userTokenToken = '22e1adf717b169ef16e12899962cb45e';

        $userToken
            ->method('getCreationDate')
            ->willReturn($userTokenCreationDate);

        $userToken
            ->method('getToken')
            ->willReturn($userTokenToken);

        $tokenService
            ->expects($this->once())
            ->method('create')
            ->willReturn($userToken);

        $mailer
            ->expects($this->once())
            ->method('send')
            ->will(
                $this->returnCallback(function(\Swift_Message $message) use ($email, $userTokenToken, $username) {
                    $this->assertEquals($email, array_keys($message->getTo())[0]);
                    $this->assertContains($userTokenToken, $message->getBody());
                    $this->assertContains($username, $message->getBody());
                })
            );

        $entityManager
            ->expects($this->once())
            ->method('flush');

        $userService = new UserService($entityManager, $tokenService, $mailer);

        $registeredUser = $userService->register($email, $username, $clientInformation);

        $this->assertEquals($email, $registeredUser->getEmail());
        $this->assertEquals($username, $registeredUser->getUsername());
    }
}