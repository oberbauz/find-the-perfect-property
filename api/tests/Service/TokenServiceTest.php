<?php

namespace App\Tests\Service;


use App\Entity\User;
use App\Exception\TokenExpiredException;
use App\Model\ClientInformation;
use App\Model\ClientToken;
use App\Model\UserToken;
use App\Service\TokenService;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Monolog\Logger;

class TokenServiceTest extends TestCase
{
    public function testBasicFreshTokenCreation()
    {
        $logger = $this->createMock(Logger::class);

        $user = $this->createMock(User::class);
        $user
            ->expects($this->exactly(2))
            ->method('getEmail')
            ->willReturn('user@example.com');

        $clientInformation = $this->createMock(ClientInformation::class);
        $clientInformation
            ->expects($this->exactly(2))
            ->method('flatten')
            ->willReturn('');

        $tokenService = new TokenService($logger);
        $creationDate = new \DateTimeImmutable();

        $token = $tokenService->create($user, $clientInformation, $creationDate);

        $this->assertInstanceOf(UserToken::class, $token);
        $this->assertEquals($creationDate, $token->getCreationDate());
    }

    public function testTokenCreationWithUserUpdate()
    {
        $logger = $this->createMock(Logger::class);

        $user = $this->createMock(User::class);
        $user
            ->expects($this->exactly(2))
            ->method('getEmail')
            ->willReturn('user@example.com');
        $user
            ->expects($this->once())
            ->method('setLastTokenCreatedAt');

        $clientInformation = $this->createMock(ClientInformation::class);
        $clientInformation
            ->expects($this->exactly(2))
            ->method('flatten')
            ->willReturn('');

        $tokenService = new TokenService($logger);

        $token = $tokenService->create($user, $clientInformation);

        $this->assertInstanceOf(UserToken::class, $token);
    }

    public function testTokenValidation()
    {
        $logger = $this->createMock(Logger::class);

        $user = $this->createMock(User::class);
        $user
            ->expects($this->exactly(2))
            ->method('getEmail')
            ->willReturn('user@example.com');

        $clientInformation = $this->createMock(ClientInformation::class);
        $clientInformation
            ->expects($this->exactly(2))
            ->method('flatten')
            ->willReturn('_-_-_');

        $tokenService = new TokenService($logger);
        $creationDate = new \DateTimeImmutable('-12 hours');

        $token = $tokenService->create($user, $clientInformation, $creationDate);

        $user
            ->expects($this->once())
            ->method('getLastTokenCreatedAt')
            ->willReturn($token->getCreationDate());

        $clientToken = $this->createMock(ClientToken::class);

        $this->expectException(TokenExpiredException::class);
        $tokenService->validateToken($user, $clientToken, $clientInformation);
    }
}