<?php
declare(strict_types = 1);

namespace App\DataFixtures;

use App\Entity\BookingRequest;
use App\Entity\Property;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AppFixtures for a complete set of test data.
 *
 * @package App\DataFixtures
 * @author  Chris Nissen <info@sixis-media.de>
 */
class AppFixtures extends Fixture
{


    /**
     * Load all configured data fixtures.
     *
     * @param ObjectManager $manager The object manager used by symfony.
     *
     * @throws \Exception
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $user1 = (new User())
            ->setUsername('tester')
            ->setEmail('tester@mcnn.de')
            ->setLastTokenCreatedAt(new \DateTimeImmutable('-1 week'));

        $user2 = (new User())
            ->setUsername('beta')
            ->setEmail('beta@mcnn.de')
            ->setLastTokenCreatedAt(new \DateTimeImmutable('-3 days'));

        $manager->persist($user1);
        $manager->persist($user2);

        $property1 = (new Property())
            ->setTitle('K+K Hotel Picasso El Born')
            ->setAddress('Passeig de Picasso, 26, 30, Barcelona')
            ->setCity('Barcelona')
            ->setPlaceId('ChIJc2TJ9P2ipBIRA65Mte-Mak8')
            ->setPositionLat(41.38675359)
            ->setPositionLong(2.1839293);

        $property2 = (new Property())
            ->setTitle('Hotel Opera')
            ->setAddress('St.-Anna-Straße 10, München')
            ->setCity('München')
            ->setPlaceId('ChIJvc36NY91nkcRWoKIMQgQBIc')
            ->setPositionLat(48.1388378)
            ->setPositionLong(11.5870394);

        $manager->persist($property1);
        $manager->persist($property2);

        $bookingRequest1 = (new BookingRequest())
            ->setCreatedAt(new \DateTimeImmutable('-17 hours'))
            ->setPersons(3)
            ->setProperty($property1)
            ->setUser($user1);

        $bookingRequest2 = (new BookingRequest())
            ->setCreatedAt(new \DateTimeImmutable('-50 minutes'))
            ->setPersons(1)
            ->setProperty($property1)
            ->setUser($user2);

        $bookingRequest3 = (new BookingRequest())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setPersons(1)
            ->setProperty($property2)
            ->setUser($user1);

        $manager->persist($bookingRequest1);
        $manager->persist($bookingRequest2);
        $manager->persist($bookingRequest3);

        $manager->flush();

    }//end load()


}//end class
