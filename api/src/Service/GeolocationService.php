<?php
declare(strict_types = 1);

namespace App\Service;


use App\Exception\LocationNotFoundException;
use GuzzleHttp\Client;

/**
 * Class GeolocationService. A class used to get the exact geolocation for a place.
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class GeolocationService
{
    /**
     * The guzzle client already set with the base uri.
     *
     * @var Client
     */
    private $googleMapsApiClient;


    /**
     * GeolocationService constructor.
     */
    public function __construct()
    {
        $this->googleMapsApiClient = new Client(
            [
                // Base URI is used with relative requests.
                'base_uri' => 'https://maps.googleapis.com',
                // You can set any number of default request options.
                'timeout'  => 2.0,
            ]
        );

    }//end __construct()


    /**
     * Get the geolocation for a given search string (normally a city name).
     *
     * @param string $search The searched location by the user.
     *
     * @throws LocationNotFoundException
     * @return array
     */
    public function getGeoLocationForLocationSearch(string $search): array
    {
        $response = $this->googleMapsApiClient->get(
            '/maps/api/geocode/json',
            [
                'query' => [
                    'components' => 'locality:'.urlencode($search),
                    'fields'     => 'geometry/location',
                    'key'        => $_ENV['GEOLOCATION_API_KEY'],
                ],
            ]
        );

        $responseBody = \GuzzleHttp\json_decode($response->getBody()->getContents());

        if ($responseBody->status === 'ZERO_RESULTS') {
            throw new LocationNotFoundException('No place for the supplied search can be found!');
        }

        return (array) $responseBody->results[0]->geometry->location;

    }//end getGeoLocationForLocationSearch()


}//end class
