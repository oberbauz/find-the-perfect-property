<?php
declare(strict_types = 1);

namespace App\Service;


use App\Entity\User;
use App\Exception\InvalidTokenException;
use App\Exception\TokenExpiredException;
use App\Model\ClientInformation;
use App\Model\ClientToken;
use App\Model\UserToken;
use Psr\Log\LoggerInterface;

/**
 * Class TokenService.
 * The TokenService class is used to generate and validate tokens used by users to login.
 *
 * @package App\Service
 * @author  Chris Nissen <info@sixis-media.de>
 */
class TokenService
{
    /**
     * The logger used to debug the token generation process.
     *
     * @var LoggerInterface
     */
    private $logger;


    /**
     * TokenService constructor.
     *
     * @param LoggerInterface $logger The logger used to debug the token generation process.
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

    }//end __construct()


    /**
     * Create a new token for the given user.
     * If no creationDate is provided a new one will be created and saved to the user.
     *
     * @param User                    $user              The user that will be able to login with the generated token.
     * @param ClientInformation       $clientInformation The public client information used to secure the token.
     * @param \DateTimeImmutable|null $creationDate      The creation date of the token,
     *                                                   if null is provided a new one is generated.
     *
     * @return UserToken
     */
    public function create(User $user, ClientInformation $clientInformation, ?\DateTimeImmutable $creationDate=null)
    {
        if ($creationDate === null) {
            $creationDate = new \DateTimeImmutable();
            $user->setLastTokenCreatedAt($creationDate);

            $this->logger->debug('New token creation date is set on the User', [$user]);
        }

        return $this->getToken($user, $clientInformation, $creationDate);

    }//end create()


    /**
     * Validate a given ClientToken.
     *
     * @param User              $user              The User that provided the token.
     * @param ClientToken       $token             The ClientToken retrieved from the browser of the Client.
     * @param ClientInformation $clientInformation The ClientInformation used to secure the token.
     *
     * @return bool
     * @throws TokenExpiredException
     * @throws InvalidTokenException
     */
    public function validateToken(User $user, ClientToken $token, ClientInformation $clientInformation): bool
    {
        $diff = $user->getLastTokenCreatedAt()->diff(new \DateTimeImmutable());

        $hours = ((int) $diff->format('%h') + ((int) $diff->format('%a') * 24));
        if ($hours > 11) {
            // Last issued token is now invalid!
            throw new TokenExpiredException('The last token was issued over 12 hours ago!');
        }

        $generatedToken = $this->getToken($user, $clientInformation, $user->getLastTokenCreatedAt());

        $this->logger->debug(
            'Token comparison',
            [
                $generatedToken->getToken(),
                $token->getToken(),
            ]
        );

        if ($generatedToken->getToken() !== $token->getToken()) {
            // The token does not match! Login invalid!
            throw new InvalidTokenException('The supplied token was invalid!');
        }

        return true;

    }//end validateToken()


    /**
     * The method used to encode all relevant information into the token.
     * This could be vastly improved!
     *
     * @param User               $user              The User the token will be generated for.
     * @param ClientInformation  $clientInformation The public client information of the user.
     * @param \DateTimeImmutable $creationDate      The creation date of the token.
     *
     * @return UserToken
     */
    private function getToken(
        User $user,
        ClientInformation $clientInformation,
        \DateTimeImmutable $creationDate
    ): UserToken {
        $token = md5($user->getEmail().$_ENV['APP_SECRET'].$clientInformation->flatten().$creationDate->format('U'));

        $this->logger->debug(
            'Token calculated',
            [
                $token,
                $user->getEmail(),
                $_ENV['APP_SECRET'],
                $clientInformation->flatten(),
                $creationDate->format('U'),
            ]
        );

        return new UserToken(
            $creationDate,
            $token
        );

    }//end getToken()


}//end class
