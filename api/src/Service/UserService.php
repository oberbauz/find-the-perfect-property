<?php
declare(strict_types = 1);

namespace App\Service;

use App\Entity\User;
use App\Exception\InvalidEmailAddress;
use App\Exception\InvalidTokenException;
use App\Exception\InvalidUsernameException;
use App\Exception\TokenExpiredException;
use App\Exception\UsernameExistsException;
use App\Exception\UserNotFoundException;
use App\Model\ClientInformation;
use App\Model\ClientToken;
use App\Model\UserToken;
use App\Repository\UserRepository;
use App\Util\Validator\EmailValidator;
use App\Util\Validator\UsernameValidator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserService
 *
 * @package App\Service
 * @author  Chris Nissen <info@sixis-media.de>
 */
class UserService
{
    /**
     * The entity manager used to retrieve entities from the database.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * The TokenService used to generate and validate tokens.
     *
     * @var TokenService
     */
    private $tokenService;

    /**
     * Mailer service used to send mails.
     *
     * @var \Swift_Mailer
     */
    private $mailer;


    /**
     * UserService constructor.
     *
     * @param EntityManagerInterface $entityManager The entity manager used to retrieve entities from the database.
     * @param TokenService           $tokenService  The TokenService used to generate and validate tokens.
     * @param \Swift_Mailer          $mailer        Mailer service used to send mails.
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TokenService $tokenService,
        \Swift_Mailer $mailer
    ) {
        $this->entityManager = $entityManager;
        $this->tokenService  = $tokenService;
        $this->mailer        = $mailer;

    }//end __construct()


    /**
     * Register a new User or send login information to the given email address.
     * In the registration process a new token will be generated that is sent to the email address provided by the user.
     *
     * @param string            $email             The email address used to register and authenticate the user.
     * @param string            $username          The desired username unique to the user.
     * @param ClientInformation $clientInformation A object containing all client information used to identify a client.
     *
     * @return User
     * @throws InvalidEmailAddress
     * @throws InvalidUsernameException
     * @throws UsernameExistsException
     */
    public function register(string $email, string $username, ClientInformation $clientInformation): User
    {
        try {
            // If a User tries to register with an existing email address he will receive a regular login mail.
            $user = $this->findUserByMail($email);
        } catch (UserNotFoundException $exception) {
            // Validate the email provided by the User. Exceptions will be thrown if email is invalid.
            EmailValidator::validate($email);

            // Validate the username.
            UsernameValidator::validate($username);

            if ($this->entityManager->getRepository(User::class)->findUserByUsername($username) !== null) {
                throw new UsernameExistsException('The provided username is already in use.');
            }

            $user = (new User())
                ->setUsername($username)
                ->setEmail($email);

            $this->entityManager->persist($user);
        }

        $token = $this->tokenService->create($user, $clientInformation);
        $this->sendLoginInformation($user, $token);

        $this->entityManager->flush();

        return $user;

    }//end register()


    /**
     * Send email containing the current token as well as teh username for authentication.
     *
     * @param User      $user      The user that requested a login.
     * @param UserToken $userToken The newly generated token for the login.
     *
     * @return void
     */
    private function sendLoginInformation(User $user, UserToken $userToken): void
    {
        // @codingStandardsIgnoreStart
        $message = (new \Swift_Message('Login Information for FTPP'))
            ->setFrom('ftpp@mcnn.de')
            ->setTo($user->getEmail())
            ->setBody(
                'Hey '.$user->getUsername().','.
                PHP_EOL.PHP_EOL.
                'a login was requested with this email address on ftpp.mcnn.de at '.
                $userToken->getCreationDate()->format('d.m.Y H:i').'.'.
                PHP_EOL.PHP_EOL.
                'If you did not request this login, please ignore this email! If you requested the login '.
                'open the following Link or copy it into your browser where the login was requested.'.
                PHP_EOL.
                '/list.html?token='.$userToken->getToken().'&username='.$user->getUsername().
                PHP_EOL.PHP_EOL.
                'FTPP'.PHP_EOL.
                'This message was sent automatically please DO NOT ANSWER!'
            );
        // @codingStandardsIgnoreEnd
        $this->mailer->send($message);

    }//end sendLoginInformation()


    /**
     * Authenticate a user with his token and client information.
     *
     * @param string            $username          The username for teh login.
     * @param ClientToken       $token             The token retrieved from the client.
     * @param ClientInformation $clientInformation The public client information used for securing the token.
     *
     * @return bool true If the login succseeds true is returned.
     * @throws UserNotFoundException
     * @throws InvalidTokenException
     */
    public function login(string $username, ClientToken $token, ClientInformation $clientInformation): ?User
    {
        /* @var User $user */
        $user = $this->entityManager
            ->getRepository(User::class)
            ->findUserByUsername($username);

        if ($user === null) {
            throw new UserNotFoundException('Can\'t login a not existing user!');
        }

        try {
            $this->tokenService->validateToken($user, $token, $clientInformation);

            return $user;
        } catch (TokenExpiredException $exception) {
            $newToken = $this->tokenService->create($user, $clientInformation);
            $this->sendLoginInformation($user, $newToken);

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return null;

    }//end login()


    /**
     * Find an User by his email address.
     *
     * @param string $email Email address of a user.
     *
     * @throws UserNotFoundException
     * @return User
     */
    private function findUserByMail(string $email): User
    {
        /* @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);

        $user = $userRepository->findByMail($email);

        if ($user === null) {
            throw new UserNotFoundException('The user with the provided email "'.$email.'" could not be found!');
        }

        return $user;

    }//end findUserByMail()


}//end class
