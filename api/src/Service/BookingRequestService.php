<?php
declare(strict_types = 1);

namespace App\Service;


use App\Entity\BookingRequest;
use App\Entity\Property;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BookingRequestService.
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class BookingRequestService
{
    /**
     * The entity manager used to retrieve entities from the database.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * BookingRequestService constructor.
     *
     * @param EntityManagerInterface $entityManager The entity manager used to retrieve entities from the database.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

    }//end __construct()


    /**
     * Create a new BookingRequest fot the given User and Property.
     *
     * @param User     $user     The user that requested this BookingRequest.
     * @param Property $property The Property that is requested.
     * @param int      $persons  The amount of persons the user is traveling with.
     *
     * @return BookingRequest
     * @throws \Exception
     */
    public function createNewBookingRequest(User $user, Property $property, int $persons=1): BookingRequest
    {
        $bookingRequest = new BookingRequest();
        $bookingRequest
            ->setPersons($persons)
            ->setCreatedAt(new \DateTimeImmutable());

        $user->addBookingRequest($bookingRequest);
        $property->addBookingRequest($bookingRequest);

        $this->entityManager->persist($bookingRequest);
        $this->entityManager->persist($user);
        $this->entityManager->persist($property);

        $this->entityManager->flush();

        return $bookingRequest;

    }//end createNewBookingRequest()


}//end class
