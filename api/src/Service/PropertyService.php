<?php
declare(strict_types = 1);

namespace App\Service;


use App\Entity\Property;
use App\Entity\User;
use App\Exception\LocationNotFoundException;
use App\Model\ApiProperty;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;

/**
 * Class PropertyService.
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class PropertyService
{
    /**
     * The GeolocationService that is used to get the exact geolocation for a user search.
     *
     * @var GeolocationService
     */
    private $geolocationService;

    /**
     * The BookingRequestService used to book a property.
     *
     * @var BookingRequestService
     */
    private $bookingRequestService;

    /**
     * The guzzle client already set with the base uri.
     *
     * @var Client
     */
    private $googleMapsApiClient;

    /**
     * The entity manager used to retrieve entities from the database.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * PropertyService constructor.
     *
     * @param GeolocationService     $geolocationService    The GeolocationService that is used to get the exact
     *                                                      geolocation for a user search.
     * @param BookingRequestService  $bookingRequestService The BookingRequestService used to book a property.
     * @param EntityManagerInterface $entityManager         The entity manager used to retrieve entities from the
     *                                                      database.
     */
    public function __construct(
        GeolocationService $geolocationService,
        BookingRequestService $bookingRequestService,
        EntityManagerInterface $entityManager
    ) {
        $this->geolocationService    = $geolocationService;
        $this->bookingRequestService = $bookingRequestService;

        $this->googleMapsApiClient = new Client(
            [
                // Base URI is used with relative requests.
                'base_uri' => 'https://maps.googleapis.com',
                // You can set any number of default request options.
                'timeout'  => 2.0,
            ]
        );

        $this->entityManager = $entityManager;

    }//end __construct()


    /**
     * List all properties near the searched place.
     *
     * @param string $search A user supplied query string for a place (typically a city).
     *
     * @throws LocationNotFoundException
     * @return array
     */
    public function listProperties(string $search): array
    {
        $cityLocation = $this->geolocationService->getGeoLocationForLocationSearch($search);

        $response = $this->googleMapsApiClient->get(
            '/maps/api/place/nearbysearch/json',
            [
                'query' => [
                    'location' => implode(',', array_values($cityLocation)),
                    'radius'   => 1000,
                    'type'     => 'lodging',
                    'key'      => $_ENV['GEOLOCATION_API_KEY'],
                ],
            ]
        );

        $lodgeLocationsNearSearch = \GuzzleHttp\json_decode($response->getBody()->getContents())->results;

        return ApiProperty::createCollectionFromApiResults($lodgeLocationsNearSearch);

    }//end listProperties()


    /**
     * Import an API result to the local database.
     *
     * @param string $placeId The placeId used to identify the property on the API.
     *
     * @return Property
     */
    private function importSingleProperty(string $placeId): Property
    {
        $apiProperty = $this->googleMapsApiClient
            ->get(
                '/maps/api/place/details/json',
                [
                    'query' => [
                        'placeid'  => $placeId,
                        'language' => 'EN',
                        'fields'   => 'name,vicinity,place_id,geometry/location',
                        'key'      => $_ENV['GEOLOCATION_API_KEY'],
                    ],
                ]
            );

        $apiProperty = \GuzzleHttp\json_decode($apiProperty->getBody()->getContents())->result;
        $apiProperty = ApiProperty::createFromApiResult($apiProperty);

        return (new Property())
            ->setTitle($apiProperty->getName())
            ->setAddress($apiProperty->getAddress())
            ->setPlaceId($apiProperty->getPlaceId())
            ->setPositionLong($apiProperty->getPositionLong())
            ->setPositionLat($apiProperty->getPositionLat());

    }//end importSingleProperty()


    /**
     * Book a property and import it if it isn't already imported.
     *
     * @param User   $user    The user requesting the booking.
     * @param string $placeId The placeId used to identify the place on the API.
     * @param int    $persons The amount of persons the user takes with him on his trip.
     *
     * @return Property
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function bookProperty(User $user, string $placeId, int $persons=1): Property
    {
        /* @var PropertyRepository $propertyRepository */
        $propertyRepository = $this->entityManager->getRepository(Property::class);

        $property = $propertyRepository->findPropertyByPlaceId($placeId);

        if ($property === null) {
            $property = $this->importSingleProperty($placeId);
        }

        // Create a new BookingRequest.
        // All relevant Entities will be persisted in the createNewBookingRequest method.
        $this->bookingRequestService->createNewBookingRequest($user, $property, $persons);

        return $property;

    }//end bookProperty()


}//end class
