<?php
declare(strict_types = 1);

namespace App\Util\Validator;


use App\Exception\InvalidUsernameException;

/**
 * Class UsernameValidator.
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class UsernameValidator implements ValidatorInterface
{


    /**
     * Validate a given username.
     *
     * @param string $value The username that should be validated.
     *
     * @return bool
     * @throws InvalidUsernameException
     */
    public static function validate($value): bool
    {
        if ($value === '' || preg_match('/[^A-Za-z0-9]/', $value) !== 0) {
            throw new InvalidUsernameException(
                'The username must be an alphanumeric string. Possible characters are a-z and the numbers 0-9.'
            );
        }

        return true;

    }//end validate()


}//end class
