<?php
declare(strict_types = 1);

namespace App\Util\Validator;


use App\Exception\InvalidEmailAddress;

/**
 * Class EmailValidator.
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class EmailValidator implements ValidatorInterface
{


    /**
     * Validate a given email address.
     *
     * @param string $value The email address that should be validated.
     *
     * @return bool
     * @throws InvalidEmailAddress
     */
    public static function validate($value): bool
    {
        if (strpos($value, '@') === -1) {
            throw new InvalidEmailAddress('The email address must contain the "@" sign.');
        }

        $emailParts = explode('@', $value);

        if (count($emailParts) > 2) {
            throw new InvalidEmailAddress('The email address must contain exactly one "@" sign.');
        } else if (filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
            throw new InvalidEmailAddress('The email address must be in a valid format.');
        }

        return true;

    }//end validate()


}//end class
