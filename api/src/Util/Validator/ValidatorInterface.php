<?php
declare(strict_types = 1);

namespace App\Util\Validator;


/**
 * Interface ValidatorInterface.
 *
 * @package App\Util\Validator
 * @author  Chris Nissen <info@sixis-media.de>
 */
interface ValidatorInterface
{


    /**
     * A generic API for validating attributes against rules.
     *
     * @param mixed $value The value that should be validated.
     *
     * @return bool
     */
    public static function validate($value): bool;


}//end interface
