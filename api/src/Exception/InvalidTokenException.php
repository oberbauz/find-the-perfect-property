<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class InvalidTokenException.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class InvalidTokenException extends ApiInvalidArgumentException
{

}//end class
