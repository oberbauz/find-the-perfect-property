<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class TokenExpiredException.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class TokenExpiredException extends ApiInvalidArgumentException
{

}//end class
