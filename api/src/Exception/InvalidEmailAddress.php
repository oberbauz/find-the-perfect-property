<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class InvalidEmailAddress.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class InvalidEmailAddress extends ApiInvalidArgumentException
{

}//end class
