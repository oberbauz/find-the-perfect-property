<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class InvalidUsernameException.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class InvalidUsernameException extends ApiInvalidArgumentException
{

}//end class
