<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class UsernameExistsException.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class UsernameExistsException extends ApiInvalidArgumentException
{

}//end class
