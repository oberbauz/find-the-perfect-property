<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class RequiredArgumentMissingException.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class RequiredArgumentMissingException extends ApiInvalidArgumentException
{

}//end class
