<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class ApiInvalidArgumentException.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class ApiInvalidArgumentException extends ApiException
{

}//end class
