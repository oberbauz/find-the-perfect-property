<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class UserNotFoundException.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class UserNotFoundException extends ApiException
{

}//end class
