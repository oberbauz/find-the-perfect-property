<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class ApiException.
 * The root Exception class for Api Exceptions.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class ApiException extends \Exception
{
}//end class
