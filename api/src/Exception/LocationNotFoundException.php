<?php
declare(strict_types = 1);

namespace App\Exception;


/**
 * Class LocationNotFoundException.
 *
 * @package App\Exception
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class LocationNotFoundException extends ApiException
{

}//end class
