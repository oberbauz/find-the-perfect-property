<?php
declare(strict_types = 1);

namespace App\Model;


use Symfony\Component\HttpFoundation\Request;

/**
 * The ClientInformation class contains all client information that is used to authenticate a client against the API.
 *
 * @package App\Service
 * @author  Chris Nissen <info@sixis-media.de>
 */
class ClientInformation
{
    /**
     * The IP Address of the client requesting a login.
     *
     * @var string
     */
    private $ipAddress;

    /**
     * The useragent of the browser.
     *
     * @var string
     */
    private $useragent;

    /**
     * A random client generated ID.
     *
     * @var string
     */
    private $clientId;


    /**
     * Private ClientInformation constructor.
     *
     * @param string $ipAddress The IP-Address of the client.
     * @param string $useragent The useragent of teh client.
     * @param string $clientId  The client-id created by the ftpp frontend code.
     */
    private function __construct(string $ipAddress, string $useragent, string $clientId)
    {
        $this->ipAddress = $ipAddress;
        $this->useragent = $useragent;
        $this->clientId  = $clientId;

    }//end __construct()


    /**
     * Return all ClientInformation data as flatt string.
     *
     * @return string
     */
    public function flatten(): string
    {
        return $this->ipAddress.$this->useragent.$this->clientId;

    }//end flatten()


    /**
     * Instantiate a new ClientInformation object from a Request object containing the required headers.
     *
     * @param Request $request The request containing the required headers.
     *
     * @return ClientInformation
     * @throws \InvalidArgumentException
     */
    public static function createFromRequest(Request $request): self
    {
        $ipAddress = $request->getClientIp();
        $useragent = $request->headers->get('user-agent');
        $clientId  = $request->headers->get('x-client-id');

        if ($ipAddress === null || $useragent === null || $clientId === null) {
            throw new \InvalidArgumentException(
                'One of the required information about the client can\'t be retrieved!'
            );
        }

        return new self($ipAddress, $useragent, $clientId);

    }//end createFromRequest()


}//end class
