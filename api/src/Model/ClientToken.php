<?php
declare(strict_types = 1);

namespace App\Model;


use Symfony\Component\HttpFoundation\Request;

/**
 * Class ClientToken.
 * This class represents a token sent by the client to authenticate on the API.
 *
 * @package App\Model
 * @author  Chris Nissen <info@sixis-media.de>
 */
class ClientToken
{
    /**
     * The string representation of the token.
     *
     * @var string
     */
    private $token;


    /**
     * ClientToken constructor.
     * The ClientToken constructor is protected since this class is extended in the UserToken class.
     *
     * @param string $token The string representation of the token.
     */
    protected function __construct(string $token)
    {
        $this->token = $token;

    }//end __construct()


    /**
     * Getter for the token.
     *
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;

    }//end getToken()


    /**
     * Create a new ClientToken instance from a request object.
     *
     * @param Request $request A request containing the required token header.
     *
     * @return ClientToken
     */
    public static function createFromRequest(Request $request): self
    {
        $clientToken = $request->headers->get('x-token');

        if ($clientToken === null || $clientToken === '') {
            throw new \InvalidArgumentException('No token found in the request!');
        }

        return new self($clientToken);

    }//end createFromRequest()


}//end class
