<?php
declare(strict_types = 1);

namespace App\Model;

/**
 * Class UserToken.
 * A on this server generated token where the creationDate will be persisted to teh User.
 *
 * @package App\Model
 * @author  Chris Nissen <info@sixis-media.de>
 */
class UserToken extends ClientToken
{
    /**
     * The creation date of the token, this is used to check expiration.
     *
     * @var \DateTimeImmutable
     */
    private $creationDate;


    /**
     * UserToken constructor.
     *
     * @param \DateTimeImmutable $creationDate The creation date of the token, this is used to check expiration.
     * @param string             $token        The token as plain string.
     */
    public function __construct(\DateTimeImmutable $creationDate, string $token)
    {
        parent::__construct($token);
        $this->creationDate = $creationDate;

    }//end __construct()


    /**
     * Getter for the creation date of the token.
     *
     * @return \DateTimeImmutable
     */
    public function getCreationDate(): \DateTimeImmutable
    {
        return $this->creationDate;

    }//end getCreationDate()


}//end class
