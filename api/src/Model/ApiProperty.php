<?php
declare(strict_types = 1);

namespace App\Model;


/**
 * Class ApiProperty.
 * This property is used to display property lists in the frontend because not all properties are imported.
 *
 * @author Chris Nissen <info@sixis-media.de>
 */
class ApiProperty implements \JsonSerializable
{
    /**
     * The unique place id used to select the associated place from the API.
     *
     * @var string
     */
    private $placeId;

    /**
     * The name of the property.
     *
     * @var string
     */
    private $name;

    /**
     * The address of the property.
     *
     * @var string
     */
    private $address;

    /**
     * The latitude of the property position.
     *
     * @var float
     */
    private $positionLat;

    /**
     * The longitude of the property position.
     *
     * @var float
     */
    private $positionLong;


    /**
     * ApiProperty constructor.
     *
     * @param string $placeId      The unique place id used to select the associated place from the API.
     * @param string $name         The name of the property.
     * @param string $address      The address of the property.
     * @param float  $positionLat  The latitude of the property position.
     * @param float  $positionLong The longitude of the property position.
     */
    private function __construct(
        string $placeId,
        string $name,
        string $address,
        float $positionLat,
        float $positionLong
    ) {
        $this->placeId      = $placeId;
        $this->name         = $name;
        $this->address      = $address;
        $this->positionLat  = $positionLat;
        $this->positionLong = $positionLong;

    }//end __construct()


    /**
     * Method for enabling json serialization.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'placeId'      => $this->placeId,
            'name'         => $this->name,
            'address'      => $this->address,
            'positionLat'  => $this->positionLat,
            'positionLong' => $this->positionLong,
        ];

    }//end jsonSerialize()


    /**
     * Create a new ApiProperty with the single result from the Google places API.
     *
     * @param object $apiResult A Api result fro the google places API.
     *
     * @return ApiProperty
     */
    public static function createFromApiResult(object $apiResult)
    {
        // phpcs:ignore
        $placeId = $apiResult->place_id;
        $name    = $apiResult->name;
        $address = $apiResult->vicinity;

        $position     = $apiResult->geometry->location;
        $positionLat  = $position->lat;
        $positionLong = $position->lng;

        return new self($placeId, $name, $address, $positionLat, $positionLong);

    }//end createFromApiResult()


    /**
     * Create a collection of ApiProperties with the result collection from the Google places API.
     *
     * @param array $apiResults Result collection from the Google places API.
     *
     * @return array
     */
    public static function createCollectionFromApiResults(array $apiResults): array
    {
        $propertyCollection = [];

        foreach ($apiResults as $apiResult) {
            $propertyCollection[] = self::createFromApiResult($apiResult);
        }

        return $propertyCollection;

    }//end createCollectionFromApiResults()


    /**
     * Getter for the placeId attribute.
     *
     * @return string
     */
    public function getPlaceId(): string
    {
        return $this->placeId;

    }//end getPlaceId()


    /**
     * Getter for the name attribute.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;

    }//end getName()


    /**
     * Getter for the address attribute.
     *
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;

    }//end getAddress()


    /**
     * Getter for the positionLat attribute.
     *
     * @return float
     */
    public function getPositionLat(): float
    {
        return $this->positionLat;

    }//end getPositionLat()


    /**
     * Getter for the positionLong attribute.
     *
     * @return float
     */
    public function getPositionLong(): float
    {
        return $this->positionLong;

    }//end getPositionLong()


}//end class
