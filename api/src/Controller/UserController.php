<?php
declare(strict_types = 1);

namespace App\Controller;


use App\Exception\ApiException;
use App\Exception\RequiredArgumentMissingException;
use App\Model\ClientInformation;
use App\Model\ClientToken;
use App\Repository\BookingRequestRepository;
use App\Service\PropertyService;
use App\Service\UserService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 *
 * @package App\Controller
 * @author  Chris Nissen <info@sixis-media.de>
 */
class UserController extends BaseController
{


    /**
     * User register action.
     *
     * @param Request         $request     The request that wil be used to extract the client information.
     * @param UserService     $userService The UserService is used to actually create the User object etc.
     * @param LoggerInterface $logger      A logger for debugging purposes.
     *
     * @return JsonResponse
     *
     * @Route("/users/register", methods={"POST","OPTIONS"})
     */
    public function register(Request $request, UserService $userService, LoggerInterface $logger): JsonResponse
    {
        $logger->info('register action invoked!');
        $clientInformation = ClientInformation::createFromRequest($request);

        $errorResponse = static::checkRequiredRequestData(['email'], $request);

        if ($errorResponse !== null) {
            return $errorResponse;
        }

        $requestData = \GuzzleHttp\json_decode($request->getContent(), true);

        $email = $requestData['email'];

        if (isset($requestData['username']) === true) {
            $username = $requestData['username'];
        } else {
            // The username is optional since this endpoint is also used for the login.
            $username = '';
        }

        $logger->debug(
            'login was called with the following parameters',
            [
                'email'             => $email,
                'username'          => $username,
                'clientInformation' => $clientInformation,
            ]
        );

        try {
            $userService->register($email, $username, $clientInformation);
        } catch (ApiException $exception) {
            return new JsonResponse(
                [
                    'status'  => 'fail',
                    'message' => $exception->getMessage(),
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        return new JsonResponse(['status' => 'ok']);

    }//end register()


    /**
     * User login action.
     *
     * @param Request         $request     The request object containing the token header to authenticate.
     * @param UserService     $userService The UserService is used to execute the login action.
     * @param LoggerInterface $logger      A logger for debugging the login action.
     *
     * @return JsonResponse
     *
     * @Route("/users/login", methods={"POST","OPTIONS"})
     */
    public function login(Request $request, UserService $userService, LoggerInterface $logger): JsonResponse
    {
        $logger->info('login action invoked!');
        $clientInformation = ClientInformation::createFromRequest($request);
        $token = ClientToken::createFromRequest($request);

        $errorResponse = static::checkRequiredRequestData(['username'], $request);

        if ($errorResponse !== null) {
            return $errorResponse;
        }

        $username = \GuzzleHttp\json_decode($request->getContent(), true)['username'];

        $logger->debug(
            'login was called with the following parameters',
            [
                'clientInformation' => $clientInformation,
                'token'             => $token->getToken(),
                'username'          => $username,
            ]
        );

        try {
            $userService->login($username, $token, $clientInformation);
        } catch (ApiException $exception) {
            $logger->notice(
                'login action failed',
                [
                    'exception' => $exception,
                    'client'    => $clientInformation,
                ]
            );
            return new JsonResponse(['status' => 'failed'], Response::HTTP_FORBIDDEN);
        }

        return new JsonResponse(['status' => 'ok']);

    }//end login()


    /**
     * Book a property for a User.
     *
     * @param string          $username        The username of the user who wants to book a property.
     * @param string          $propertyId      The placeId provided by the API to identify the property.
     * @param Request         $request         The request of the User.
     * @param PropertyService $propertyService The property service that is used to list all properties.
     * @param UserService     $userService     The UserService used to authenticate the user.
     *
     * @return JsonResponse
     *
     * @Route("/users/{username}/book/{propertyId}", methods={"POST","OPTIONS"})
     */
    public function book(
        string $username,
        string $propertyId,
        Request $request,
        PropertyService $propertyService,
        UserService $userService
    ): JsonResponse {
        try {
            $clientInformation = ClientInformation::createFromRequest($request);
            $token = ClientToken::createFromRequest($request);

            $user = $userService->login($username, $token, $clientInformation);
        } catch (ApiException $exception) {
            return new JsonResponse(['status' => 'failed'], Response::HTTP_FORBIDDEN);
        }

        // At this point the user is successfully authenticated.
        $propertyService->bookProperty($user, $propertyId);

        return new JsonResponse(
            ['status' => 'ok']
        );

    }//end book()


    /**
     * List all bookings for a User.
     *
     * @param string                   $username                 The username of the user who wants to book a property.
     * @param Request                  $request                  The request of the User.
     * @param BookingRequestRepository $bookingRequestRepository The BookingRequestRepository.
     * @param UserService              $userService              The UserService used to authenticate the user.
     *
     * @return JsonResponse
     *
     * @Route("/users/{username}/bookings", methods={"GET","OPTIONS"})
     */
    public function listBookings(
        string $username,
        Request $request,
        BookingRequestRepository $bookingRequestRepository,
        UserService $userService
    ): JsonResponse {
        try {
            $clientInformation = ClientInformation::createFromRequest($request);
            $token = ClientToken::createFromRequest($request);

            $user = $userService->login($username, $token, $clientInformation);
        } catch (ApiException $exception) {
            return new JsonResponse(['status' => 'failed'], Response::HTTP_FORBIDDEN);
        }

        // At this point the user is successfully authenticated.
        $bookingRequests = $bookingRequestRepository->findAllBookingRequestsForUser($user);

        return new JsonResponse(
            [
                'status'   => 'ok',
                'bookings' => $bookingRequests,
            ]
        );

    }//end listBookings()


}//end class
