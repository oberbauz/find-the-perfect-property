<?php
declare(strict_types = 1);

namespace App\Controller;


use App\Exception\RequiredArgumentMissingException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseController
 *
 * @package App\Controller
 * @author  Chris Nissen <info@sixis-media.de>
 */
abstract class BaseController
{


    /**
     * Check if the supplied request contains all required JSON keys on the first level.
     * If any problem with teh request is encountered the appropriate error response will be returned.
     *
     * @param array|string[] $requiredKeys Simple array containing the required keys.
     * @param Request        $request      The request object.
     *
     * @return JsonResponse|null
     */
    protected static function checkRequiredRequestData(array $requiredKeys, Request $request): ?JsonResponse
    {
        try {
            $requestData = \GuzzleHttp\json_decode($request->getContent(), true);

            foreach ($requiredKeys as $requiredKey) {
                if (isset($requestData[$requiredKey]) === false) {
                    throw new RequiredArgumentMissingException('The request must contain a "'.$requiredKey.'"!');
                }
            }
        } catch (RequiredArgumentMissingException $exception) {
            return new JsonResponse(
                [
                    'status'  => 'failed',
                    'message' => $exception->getMessage(),
                ],
                Response::HTTP_BAD_REQUEST
            );
        } catch (\Exception $exception) {
            return new JsonResponse(
                [
                    'status'  => 'failed',
                    'message' => 'The request must contain valid JSON!',
                ],
                Response::HTTP_BAD_REQUEST
            );
        }//end try

        return null;

    }//end checkRequiredRequestData()


}//end class
