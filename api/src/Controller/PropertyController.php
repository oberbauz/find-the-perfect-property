<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Exception\LocationNotFoundException;
use App\Repository\PropertyRepository;
use App\Service\PropertyService;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropertyController
 *
 * @package App\Controller
 * @author  Chris Nissen <info@sixis-media.de>
 */
class PropertyController extends BaseController
{


    /**
     * List properties for search.
     *
     * @param string          $location        The location a User is searching for properties.
     * @param PropertyService $propertyService The property service that is used to list all properties.
     *
     * @return JsonResponse
     *
     * @Route("/properties/list/{location}", methods={"GET","OPTIONS"})
     */
    public function list(
        string $location,
        PropertyService $propertyService
    ): JsonResponse {
        try {
            $properties = $propertyService->listProperties($location);
        } catch (LocationNotFoundException $exception) {
            return new JsonResponse(['status' => 'fail'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(
            [
                'status' => 'ok',
                'result' => $properties,
            ]
        );

    }//end list()


    /**
     * List bookings for a property.
     *
     * @param string          $placeId            The place which should be booked.
     * @param PropertyService $propertyRepository The property service that is used to list all properties.
     *
     * @return JsonResponse
     *
     * @Route("/properties/{placeId}/bookings", methods={"GET","OPTIONS"})
     */
    public function listBookings(
        string $placeId,
        PropertyRepository $propertyRepository
    ): JsonResponse {
        $property = $propertyRepository->findPropertyByPlaceId($placeId, true);

        if ($property === null) {
            return new JsonResponse(['status' => 'fail'], Response::HTTP_NOT_FOUND);
        }

        /* @var PersistentCollection $bookingRequests */
        $bookingRequests = $property->getBookingRequests();

        return new JsonResponse(
            [
                'status'   => 'ok',
                'bookings' => $bookingRequests->getValues(),
            ]
        );

    }//end listBookings()


}//end class
