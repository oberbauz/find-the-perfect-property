<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * The UserRepository is used to select User entities from the database.
 *
 * @author Chris Nissen <info@sixis-media.de>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{


    /**
     * UserRepository constructor.
     *
     * @param RegistryInterface $registry The RegistryInterface.
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);

    }//end __construct()


    /**
     * Find an User by the email address.
     *
     * @param string $email The email address of the User.
     *
     * @return User|null
     */
    public function findByMail(string $email): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :email')
            ->setParameter('email', $email)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

    }//end findByMail()


    /**
     * Find a user by the username. If no User is found null will be returned.
     *
     * @param string $username The username of the User.
     *
     * @return User|null
     */
    public function findUserByUsername(string $username): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username = :username')
            ->setParameter('username', $username)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

    }//end findUserByUsername()


}//end class
