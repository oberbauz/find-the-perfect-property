<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Property;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * The PropertyRepository is used to select Property entities from the database.
 *
 * @author Chris Nissen <info@sixis-media.de>
 *
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRepository extends ServiceEntityRepository
{


    /**
     * PropertyRepository constructor.
     *
     * @param RegistryInterface $registry The RegistryInterface.
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Property::class);

    }//end __construct()


    /**
     * Find a property by the place id. If no Property is found null will be returned.
     *
     * @param string $placeId             The placeId provided by the API.
     * @param bool   $joinBookingRequests Set this parameter to true to preload all BookingRequests.
     *
     * @return Property|null
     */
    public function findPropertyByPlaceId(string $placeId, bool $joinBookingRequests=false): ?Property
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.placeId = :placeId')
            ->setParameter('placeId', $placeId)
            ->orderBy('p.id', 'ASC');

        if ($joinBookingRequests === true) {
            $query
                ->join('p.bookingRequests', 'br')
                ->join('br.user', 'u')
                ->addSelect('br')
                ->addSelect('u');
        } else {
            // If just a single property should be loaded set the max result flag.
            $query->setMaxResults(1);
        }

        return $query
            ->getQuery()
            ->getOneOrNullResult();

    }//end findPropertyByPlaceId()


}//end class
