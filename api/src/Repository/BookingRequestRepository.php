<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\BookingRequest;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * The BookingRequestRepository is used to select BookingRequest entities from the database.
 *
 * @author Chris Nissen <info@sixis-media.de>
 *
 * @method BookingRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingRequest[]    findAll()
 * @method BookingRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRequestRepository extends ServiceEntityRepository
{


    /**
     * BookingRequestRepository constructor.
     *
     * @param RegistryInterface $registry The RegistryInterface.
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingRequest::class);

    }//end __construct()


    /**
     * Find all BookingRequests of a User.
     *
     * @param User $user The User object with BookingRequests.
     *
     * @return BookingRequest[]|array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findAllBookingRequestsForUser(User $user): array
    {
        return $this->createQueryBuilder('br')
            ->andWhere('br.user = :user')
            ->setParameter('user', $user)
            ->join('br.property', 'p')
            ->addSelect('p')
            ->orderBy('br.id', 'ASC')
            ->getQuery()
            ->getResult();

    }//end findAllBookingRequestsForUser()


}//end class
