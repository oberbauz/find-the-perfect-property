<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * The Property entity is a clone from an object on the here.com API only containing
 * the title and the position of the property.
 *
 * @author Chris Nissen <info@sixis-media.de>
 *
 * @ORM\Entity(repositoryClass="App\Repository\PropertyRepository")
 */
class Property
{
    /**
     * The unique ID of the Property.
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $id;

    /**
     * The title of the Property e.g.: Chrysler Building
     *
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $title;

    /**
     * The latitude of the property position.
     *
     * @ORM\Column(type="float")
     *
     * @var float
     */
    private $positionLat;

    /**
     * The longitude of the property position.
     *
     * @ORM\Column(type="float")
     *
     * @var float
     */
    private $positionLong;

    /**
     * All booking requests made for this property.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\BookingRequest", mappedBy="property")
     *
     * @var ArrayCollection|BookingRequest[]
     */
    private $bookingRequests;

    /**
     * The placeId provided by the API.
     *
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $placeId;

    /**
     * The address for the property provided by the API.
     *
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $address;

    /**
     * The city literal.
     *
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $city;


    /**
     * Property constructor.
     */
    public function __construct()
    {
        $this->bookingRequests = new ArrayCollection();

    }//end __construct()


    /**
     * Getter for the ID attribute.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;

    }//end getId()


    /**
     * Getter for the title attribute.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;

    }//end getTitle()


    /**
     * Setter for the title attribute.
     *
     * @param string $title The title of the Property
     *
     * @return Property
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;

    }//end setTitle()


    /**
     * Getter for the positionLat attribute.
     *
     * @return float|null
     */
    public function getPositionLat(): ?float
    {
        return $this->positionLat;

    }//end getPositionLat()


    /**
     * Setter for the positionLat attribute.
     *
     * @param float $positionLat The latitude of the property position.
     *
     * @return Property
     */
    public function setPositionLat(float $positionLat): self
    {
        $this->positionLat = $positionLat;

        return $this;

    }//end setPositionLat()


    /**
     * Getter for the positionLong attribute.
     *
     * @return float|null
     */
    public function getPositionLong(): ?float
    {
        return $this->positionLong;

    }//end getPositionLong()


    /**
     * Setter for the positionLong attribute.
     *
     * @param float $positionLong The longitude of the Property position.
     *
     * @return Property
     */
    public function setPositionLong(float $positionLong): self
    {
        $this->positionLong = $positionLong;

        return $this;

    }//end setPositionLong()


    /**
     * Getter for the bookingRequests attribute.
     *
     * @return Collection|BookingRequest[]
     */
    public function getBookingRequests(): Collection
    {
        return $this->bookingRequests;

    }//end getBookingRequests()


    /**
     * Method for adding a BookingRequest to a Property.
     *
     * @param BookingRequest $bookingRequest The BookingRequest that should be added to the Property.
     *
     * @return Property
     */
    public function addBookingRequest(BookingRequest $bookingRequest): self
    {
        if ($this->bookingRequests->contains($bookingRequest) === false) {
            $this->bookingRequests[] = $bookingRequest;
            $bookingRequest->setProperty($this);
        }

        return $this;

    }//end addBookingRequest()


    /**
     * Method for removing a BookingRequest from a Property.
     *
     * @param BookingRequest $bookingRequest The BookingRequest that should be removed.
     *
     * @return Property
     */
    public function removeBookingRequest(BookingRequest $bookingRequest): self
    {
        if ($this->bookingRequests->contains($bookingRequest) === true) {
            $this->bookingRequests->removeElement($bookingRequest);
            // Set the owning side to null (unless already changed).
            if ($bookingRequest->getProperty() === $this) {
                $bookingRequest->setProperty(null);
            }
        }

        return $this;

    }//end removeBookingRequest()


    /**
     * Getter for the placeId attribute.
     *
     * @return string|null
     */
    public function getPlaceId(): ?string
    {
        return $this->placeId;

    }//end getPlaceId()


    /**
     * Setter for the placeId attribute.
     *
     * @param string $placeId The placeId provided by the API.
     *
     * @return Property
     */
    public function setPlaceId(string $placeId): self
    {
        $this->placeId = $placeId;

        return $this;

    }//end setPlaceId()


    /**
     * Getter for the address attribute.
     *
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;

    }//end getAddress()


    /**
     * Setter for the address attribute.
     *
     * @param string $address The address of the property.
     *
     * @return Property
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;

        $addressParts = explode(',', $address);
        $this->setCity(trim($addressParts[(count($addressParts) - 1)]));

        return $this;

    }//end setAddress()


    /**
     * Getter for the city attribute.
     *
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;

    }//end getCity()


    /**
     * Setter for the city attribute.
     *
     * @param string $city The city literal where the property is located.
     *
     * @return Property
     */
    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;

    }//end setCity()


}//end class
