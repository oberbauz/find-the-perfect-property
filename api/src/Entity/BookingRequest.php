<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * The BookingRequest entity represents the booking of a specified Property for a given User.
 *
 * @author Chris Nissen <info@sixis-media.de>
 *
 * @ORM\Entity(repositoryClass="App\Repository\BookingRequestRepository")
 */
class BookingRequest implements \JsonSerializable
{
    /**
     * The unique ID of the BookingRequest.
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $id;

    /**
     * The creation date of the BookingRequest.
     *
     * @ORM\Column(type="datetime_immutable")
     *
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * The amount of people staying at the property.
     *
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $persons;

    /**
     * The User that created the BookingRequest.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bookingRequests")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var User
     */
    private $user;

    /**
     * The Property that was booked.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Property", inversedBy="bookingRequests")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Property
     */
    private $property;


    /**
     * Return the JSON representation of the BookingRequest object.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id'               => $this->getId(),
            'property_id'      => $this->getProperty()->getPlaceId(),
            'property_name'    => $this->getProperty()->getTitle(),
            'city'             => $this->getProperty()->getCity(),
            'property_address' => $this->getProperty()->getAddress(),
            'user'             => $this->getUser(),
        ];

    }//end jsonSerialize()


    /**
     * Getter for the ID attribute.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;

    }//end getId()


    /**
     * Getter for the createdAt attribute.
     *
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;

    }//end getCreatedAt()


    /**
     * Setter for the createdAt attribute.
     *
     * @param \DateTimeInterface $createdAt The time the BookingRequest was created.
     *
     * @return BookingRequest
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;

    }//end setCreatedAt()


    /**
     * Getter for the persons attribute.
     *
     * @return int|null
     */
    public function getPersons(): ?int
    {
        return $this->persons;

    }//end getPersons()


    /**
     * Setter for the persons attribute.
     *
     * @param int $persons The amount of persons in the property.
     *
     * @return BookingRequest
     */
    public function setPersons(int $persons): self
    {
        $this->persons = $persons;

        return $this;

    }//end setPersons()


    /**
     * Getter for the user attribute.
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;

    }//end getUser()


    /**
     * Setter for the user attribute.
     *
     * @param User|null $user The User who created the BookingRequest.
     *
     * @return BookingRequest
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;

    }//end setUser()


    /**
     * Getter for the property attribute.
     *
     * @return Property|null
     */
    public function getProperty(): ?Property
    {
        return $this->property;

    }//end getProperty()


    /**
     * Setter for the property attribute.
     *
     * @param Property|null $property The Property that is booked.
     *
     * @return BookingRequest
     */
    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;

    }//end setProperty()


}//end class
