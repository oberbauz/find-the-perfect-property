<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * The User entity represents a registered User via the API. Associated with the User are his BookingRequests.
 * A given user will be able to "log in" with a random generated Access code sent to his email address.
 *
 * @author Chris Nissen <info@sixis-media.de>
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements \JsonSerializable
{
    /**
     * The unique ID of the User.
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $id;

    /**
     * The username of the User.
     *
     * @ORM\Column(type="string", length=25)
     *
     * @var string
     */
    private $username;

    /**
     * The email address of the User in order to send verification mails.
     *
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @var string
     */
    private $email;

    /**
     * All BookingRequests made by the User.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\BookingRequest", mappedBy="user", orphanRemoval=true)
     *
     * @var ArrayCollection
     */
    private $bookingRequests;

    /**
     * The issue date of the last token created for the user.
     *
     * @ORM\Column(type="datetime_immutable", nullable=true)
     *
     * @var \DateTimeImmutable
     */
    private $lastTokenCreatedAt;


    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->bookingRequests = new ArrayCollection();

    }//end __construct()


    /**
     * Return the JSON representation of the user object.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id'       => $this->getId(),
            'username' => $this->getUsername(),
        ];

    }//end jsonSerialize()


    /**
     * Getter for the ID attribute.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;

    }//end getId()


    /**
     * Getter for the username attribute.
     *
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;

    }//end getUsername()


    /**
     * Setter for the username attribute.
     *
     * @param string $username The unique username of the User.
     *
     * @return User
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;

    }//end setUsername()


    /**
     * Getter for the email attribute.
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;

    }//end getEmail()


    /**
     * Setter for the email attribute.
     *
     * @param string $email The email address used to verify the User.
     *
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;

    }//end setEmail()


    /**
     * Getter for the bookingRequests attribute.
     *
     * @return Collection|BookingRequest[]
     */
    public function getBookingRequests(): Collection
    {
        return $this->bookingRequests;

    }//end getBookingRequests()


    /**
     * Method for adding a BookingRequest to a User.
     *
     * @param BookingRequest $bookingRequest The BookingRequest that should be added to the User.
     *
     * @return User
     */
    public function addBookingRequest(BookingRequest $bookingRequest): self
    {
        if ($this->bookingRequests->contains($bookingRequest) === false) {
            $this->bookingRequests[] = $bookingRequest;
            $bookingRequest->setUser($this);
        }

        return $this;

    }//end addBookingRequest()


    /**
     * Method for removing a BookingRequest from a User.
     *
     * @param BookingRequest $bookingRequest The BookingRequest that should be removed.
     *
     * @return User
     */
    public function removeBookingRequest(BookingRequest $bookingRequest): self
    {
        if ($this->bookingRequests->contains($bookingRequest) === true) {
            $this->bookingRequests->removeElement($bookingRequest);
            // Set the owning side to null (unless already changed).
            if ($bookingRequest->getUser() === $this) {
                $bookingRequest->setUser(null);
            }
        }

        return $this;

    }//end removeBookingRequest()


    /**
     * Getter for the lastTokenCreatedAt attribute.
     *
     * @return \DateTimeImmutable|null
     */
    public function getLastTokenCreatedAt(): ?\DateTimeImmutable
    {
        return $this->lastTokenCreatedAt;

    }//end getLastTokenCreatedAt()


    /**
     * Setter for the lastTokenCreatedAt attribute.
     *
     * @param \DateTimeImmutable|null $lastTokenCreatedAt The creation date of the last issued token for this User.
     *
     * @return User
     */
    public function setLastTokenCreatedAt(?\DateTimeImmutable $lastTokenCreatedAt): self
    {
        $this->lastTokenCreatedAt = $lastTokenCreatedAt;

        return $this;

    }//end setLastTokenCreatedAt()


}//end class
