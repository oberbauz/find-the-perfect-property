
module.exports.command = function(file, callback) {
    this
        .execute(function() {
            return typeof ftppApi;
        }, [], function(result) {
            if (result.value !== 'object') {
                browser.assert.fail(
                    'The FTPP API client should be present!',
                    '',
                    'It seems like the ftppApi object isn\'t loaded properly!'
                );
            }
        });
    return this;
};
