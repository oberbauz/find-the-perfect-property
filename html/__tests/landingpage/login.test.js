var testEmail = 'E2Enewtester@mcnn.de';

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

module.exports = {
    beforeEach : function(browser) {
        browser
            .url('http://localhost')
    },
    'open login modal and try to login new user after registration' : function (browser) {
        var registeredEmail = makeid(7)+testEmail;

        browser
            .pause(400)
            .checkFtppApiObject()
            .waitForElementVisible('.navbar button.btn.btn-outline-light.d-lg-inline-block', 400)
            .click('.navbar button.btn.btn-outline-light.d-lg-inline-block')
            .waitForElementVisible('#login_modal')
            .setValue('#login_email', makeid(7)+testEmail)
            .click('#login_action')
            .pause(10)
            .assert.attributeEquals('#login_action', 'disabled', 'true')
            .waitForElementPresent('#login_form .alert', 15000) // 15s timeout due to slow API on docker/Mac
            .assert.containsText('#login_form .alert', 'Something went wrong!')
            .click('#login_form .alert a[data-toggle=modal]')
            .waitForElementVisible('#register_modal')
            .pause(100)
            .setValue('#register_email', registeredEmail)
            .setValue('#register_username', makeid(10))
            .click('#register_action')
            .pause(10)
            .assert.attributeEquals('#register_action', 'disabled', 'true')
            .waitForElementPresent('#register_form .alert', 15000) // 15s timeout due to slow API on docker/Mac
            .assert.containsText('#register_form .alert', 'You are now successfully registered!')
            .click('#register_modal button[data-dismiss=modal]')
            .pause(300)
            .click('.navbar button.btn.btn-outline-light.d-lg-inline-block')
            .waitForElementVisible('#login_modal')
            .clearValue('#login_email')
            .setValue('#login_email', registeredEmail)
            .click('#login_action')
            .pause(10)
            .assert.attributeEquals('#login_action', 'disabled', 'true')
            .waitForElementPresent('#login_form .alert.alert-success', 15000) // 15s timeout due to slow API on docker/Mac
            .pause(500)
            .assert.containsText('#login_form .alert', 'You successfully requested a login!')
            .end();
    },

    'login able to send form again after form error' : function (browser) {
        browser
            .pause(400)
            .checkFtppApiObject()
            .waitForElementVisible('.navbar button.btn.btn-outline-light.d-lg-inline-block', 400)
            .click('.navbar button.btn.btn-outline-light.d-lg-inline-block')
            .waitForElementVisible('#login_modal')
            .setValue('#login_email', '@'+testEmail)
            .click('#login_action')
            .waitForElementPresent('#login_form .alert')
            .assert.containsText('#login_form .alert', 'The form contains some errors')
            .getAttribute('#login_action', 'disabled', function(result) {
                this.assert.equal(result.value, null);
            })
            .end();
    }
};
