var testSearch = 'munich';

module.exports = {
    beforeEach : function(browser) {
        browser
            .url('http://localhost')
    },
    'execute search with all parameters' : function (browser) {
        browser
            .waitForElementVisible('input[type=text]')
            .setValue('input[type=text]', testSearch)
            .setValue('select[name=pers]', 3)
            .pause(300)
            .checkFtppApiObject()
            .click('input[type=submit]')
            .pause(1000)
            .assert.urlEquals('http://localhost/list.html?q='+testSearch+'&pers=3')
            .assert.value('#property_search_location', testSearch)
            .assert.value('#property_persons_select', '3')
            .waitForElementPresent('#list_result_container .property-list', 10000) // Ajax might be slow
            .assert.containsText(
                '#list_result_container',
                'Hotel',
                'There should be some hotels found in Munich.'
            )
            .end();
    }
};
