var testEmail = '.test@mcnn.de';
var testUsername = 'test';

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

module.exports = {
    beforeEach : function(browser) {
        browser
            .url('http://localhost/list.html')
    },
    'open register modal and register user' : function (browser) {
        browser
            .pause(400)
            .checkFtppApiObject()
            .waitForElementVisible('.navbar button.btn.btn-outline-info.d-lg-inline-block', 400)
            .click('.navbar button.btn.btn-outline-info.d-lg-inline-block')
            .waitForElementVisible('#register_modal')
            .setValue('#register_email', makeid(7)+testEmail)
            .setValue('#register_username', testUsername+makeid(7))
            .click('#register_action')
            .pause(10)
            .assert.attributeEquals('#register_action', 'disabled', 'true')
            .waitForElementPresent('#register_form .alert', 15000) // 15s timeout due to slow API on docker/Mac
            .assert.containsText('#register_form .alert', 'You are now successfully registered!')
            .end();
    },

    'register able to send form again after form error' : function (browser) {
        browser
            .pause(400)
            .checkFtppApiObject()
            .waitForElementVisible('.navbar button.btn.btn-outline-info.d-lg-inline-block', 400)
            .click('.navbar button.btn.btn-outline-info.d-lg-inline-block')
            .waitForElementVisible('#register_modal')
            .setValue('#register_email', makeid(7)+testEmail)
            .click('#register_action')
            .waitForElementPresent('#register_form .alert')
            .assert.containsText('#register_form .alert', 'The form contains some errors')
            .getAttribute('#register_action', 'disabled', function(result) {
                this.assert.equal(result.value, null);
            })
            .end();
    },

    'register with invalid username' : function (browser) {
        browser
            .pause(400)
            .checkFtppApiObject()
            .waitForElementVisible('.navbar button.btn.btn-outline-info.d-lg-inline-block', 400)
            .click('.navbar button.btn.btn-outline-info.d-lg-inline-block')
            .waitForElementVisible('#register_modal')
            .setValue('#register_email', makeid(7)+testEmail)
            .setValue('#register_username', '###')
            .click('#register_action')
            .waitForElementPresent('#register_form .alert', 15000) // 15s timeout due to slow API on docker/Mac
            .assert.containsText('#register_form .alert', 'Unfortunately we could not finish your registration.')
            .getAttribute('#register_action', 'disabled', function(result) {
                this.assert.equal(result.value, null);
            })
            .end();
    }
};
