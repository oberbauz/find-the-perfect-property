var testSearch = 'New York';
var testToken = '630ac868eda2d8f01131c5e5e94f1df9';

module.exports = {
    'execute search for non existent place' : function (browser) {
        browser
            .url('http://localhost/list.html?token='+testToken)
            .waitForElementVisible('input[type=text]')
            .setValue('input[type=text]', 'xyxyxyxyxy')
            .setValue('select[name=pers]', 2)
            .pause(300)
            .checkFtppApiObject()
            .click('input[type=submit]')
            .pause(200)
            // The token for user authentication should be preserved!
            .assert.urlEquals('http://localhost/list.html?token='+testToken+'&q=xyxyxyxyxy&pers=2')
            .waitForElementPresent('#list_result_container .alert-danger', 10000) // Ajax might be slow
            .assert.containsText(
                '#list_result_container',
                'No Results found!',
                'There shouldn\'t be any results for this initial search.'
            )
            .click('#list_result_container li:nth-child(3) a')
            .pause(1000)
            .assert.urlEquals('http://localhost/list.html?token='+testToken+'&q=Los+Angeles&pers=2')
            .assert.value('#property_search_location', 'Los Angeles')
            .assert.value('#property_persons_select', '2')
            .waitForElementPresent('#list_result_container .property-list', 10000) // Ajax might be slow
            .assert.containsText(
                '#list_result_container',
                'Hotel',
                'There should be some hotels in the result.'
            )
            .end();
    },
    'execute search with all parameters' : function (browser) {
        browser
            .url('http://localhost/list.html?token='+testToken)
            .waitForElementVisible('input[type=text]')
            .setValue('input[type=text]', testSearch)
            .setValue('select[name=pers]', 3)
            .pause(300)
            .checkFtppApiObject()
            .click('input[type=submit]')
            .pause(200)
            // The token for user authentication should be preserved!
            .assert.urlEquals('http://localhost/list.html?token='+testToken+'&q='+testSearch.split(' ').join('+')+'&pers=3')
            .waitForElementPresent('#list_result_container .property-list', 10000) // Ajax might be slow
            .assert.containsText(
                '#list_result_container',
                'Hotel',
                'There should be some hotels in the result.'
            )
            .end();
    }
};
