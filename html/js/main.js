/*jshint esversion: 6 */
$(document).ready(function() {
    ftppApi
        .addDomElement('globalAlertContainer', '#global_alert_container')
        .addDomElement('registrationForm', '#register_form')
        .addDomElement('registrationEmail', '#register_email')
        .addDomElement('registrationUsername', '#register_username')
        .addDomElement('registrationButton', '#register_action')
        .addDomElement('loginForm', '#login_form')
        .addDomElement('loginEmail', '#login_email')
        .addDomElement('loginButton', '#login_action');


    if (location.pathname === '/' || location.pathname === '/list.html') {
        // Pages have search bar - add the to the API
        ftppApi
            .addDomElement('propertySearchLocation', '#property_search_location')
            .addDomElement('propertyPersonsSelect', '#property_persons_select');
    }

    if (location.pathname === '/list.html') {
        // Load list result
        ftppApi
            .addDomElement('listResultContainer', '#list_result_container');

        ftppApi
            .setUpEventListeners('click','loadPropertyList', '#property_search_button');
    }


    if (location.pathname === '/' || location.pathname === '/list.html') {
        // Pages have search bar - add the to the API
        ftppApi
            .addDomElement('propertySearchLocation', '#property_search_location')
            .addDomElement('propertyPersonsSelect', '#property_persons_select');
    }

    if (location.pathname === '/list.html') {
        // Load list result
        ftppApi
            .addDomElement('listResultContainer', '#list_result_container');

        ftppApi
            .setUpEventListeners('click','loadPropertyList', '#property_search_button')
            .setUpEventListeners(
                'click',
                'sendBookingRequest',
                '#list_result_container .booking-request-action'
            );
    }

    if (location.pathname === '/my-bookings.html') {
        ftppApi
            .addDomElement('userBookingsResultContainer', '#my_bookings_result_container');

    }

    ftppApi.setUpEventListeners('click','sendRegistrationData', '#register_action');
    ftppApi.setUpEventListeners('click','sendLoginData', '#login_action');

    ftppApi.init();
});


// Automatically close other modals if modal link was clicked
$('body').on('click', '.modal a[data-toggle=modal]', function () {
    $('body > .modal').modal('hide');
});