/*jshint esversion: 6 */
const ftppApi = {
    axios: null,
    isLoggedIn: false,
    username: null,
    domElements: {
        globalAlertContainer: false,
        registrationForm: false,
        registrationUsername: false,
        registrationEmail: false,
        registrationButton: false,
        loginForm: false,
        loginEmail: false,
        loginButton: false,
        propertySearchLocation: false,
        propertyPersonsSelect: false,
        listResultContainer: false,
        userBookingsResultContainer: false
    },
    events: {
        sendBookingRequest: function (event) {
            var eventTarget = $(event.target);
            var bookingButton;

            if (eventTarget.hasClass('booking-request-action')) {
                bookingButton = eventTarget;
            } else {
                bookingButton = eventTarget.parents('.booking-request-action');
            }

            var propertyId = bookingButton.attr('data-ftpp-place-id');
            var propertyName = bookingButton.attr('data-ftpp-place-name');

            if (propertyId === '' || propertyId === undefined) {
                ftppApi.domElements.globalAlertContainer.append(
                    '<div class="alert alert-warning alert-dismissible fade show" role="alert">' +
                    '  <strong>Booking request failed!</strong> Something went wrong - we could not find the place id you wanted to book!' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>'
                );

                window.scrollTo({top: 0, behavior: 'smooth'});
                return false;
            }

            bookingButton.find('.original-text').addClass('d-none');
            bookingButton.prepend(
                '<span class="booking-request-loader"><span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Loading...</span>'
            );

            ftppApi.sendBookingRequest(
                propertyId,
                propertyName,
                ftppApi.username,
                ftppApi._getQueryVariable('token'),
                function (responseData) {
                    bookingButton.find('.booking-request-loader').remove();
                    var text = bookingButton.find('.original-text').removeClass('d-none');

                    if (responseData.status === 'ok') {
                        text.text('Successfully Booked');
                        bookingButton.attr('disabled', true);
                    }
                }
            );
        },
        loadPropertyList: function () {
            ftppApi._checkIfDomElementsArePresent([
                'listResultContainer',
                'propertyPersonsSelect',
                'propertySearchLocation'
            ], 'events.loadPropertyList');

            var searchElem = ftppApi.domElements.propertySearchLocation;
            var personsElem = ftppApi.domElements.propertyPersonsSelect;
            var urlSearchValue = ftppApi._getQueryVariable(searchElem[0].name);
            var urlPersonsValue = ftppApi._getQueryVariable(personsElem[0].name);
            console.debug('Load property list!');

            if (urlSearchValue !== searchElem.val()) {
                ftppApi.loadPropertyList(searchElem.val(), ftppApi.domElements.listResultContainer);
            }

            if (urlSearchValue !== searchElem.val() || urlPersonsValue !== personsElem.val()) {
                var newUrlQuery = ftppApi._setQueryVariable(searchElem[0].name, searchElem.val());
                var newUrl = window.location.origin+window.location.pathname+newUrlQuery;

                newUrlQuery = ftppApi._setQueryVariable(personsElem[0].name, personsElem.val(), newUrl);

                window.history.pushState("", "", window.location.pathname+newUrlQuery);
                console.debug('URL updated!');
            }

        },
        sendRegistrationData: function () {
            // check for required domElements
            ftppApi._checkIfDomElementsArePresent([
                'registrationForm',
                'registrationUsername',
                'registrationEmail'
            ], 'events.sendRegistrationData');

            // load required fields
            var form = ftppApi.domElements.registrationForm;
            var username = ftppApi.domElements.registrationUsername[0].value;
            var email = ftppApi.domElements.registrationEmail[0].value;

            console.debug(form);

            if(form[0].reportValidity() === true) {
                ftppApi.register(email, username);
            } else {
                form.prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '  <h6 class="alert-heading">The form contains some errors!</h6>' +
                    '  <p class="mb-0">Please check your registration input again.</p>' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>');
            }
        },
        sendLoginData: function() {
            // check for required domElements
            ftppApi._checkIfDomElementsArePresent([
                'loginForm',
                'loginEmail'
            ], 'events.sendLoginData');

            var form = ftppApi.domElements.loginForm;
            var email = ftppApi.domElements.loginEmail[0].value;

            if(form[0].reportValidity() === true) {
                ftppApi.login(email);
            } else {
                form.prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '  <h6 class="alert-heading">The form contains some errors!</h6>' +
                    '  <p class="mb-0">Please check your login email. The email address is used to authenticate your account, you must have access to the email address!</p>' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>');
            }
        }
    },
    init: function() {
        this._checkClientId();

        this.axios = axios.create({
            baseURL: 'http://localhost:8000',
            timeout: 30000,
            headers: {'X-Client-Id': localStorage.getItem('client-id')}
        });

        if (this.domElements.propertySearchLocation !== false) {
            // Since a property search bar is present we assume the property search form is existent.
            this._initSearchBar();

            var searchDomElement = this.domElements.propertySearchLocation;

            if (this.domElements.listResultContainer !== false && searchDomElement.val() !== '') {
                // Manually trigger initial load of property list
                this.loadPropertyList(searchDomElement.val(), this.domElements.listResultContainer);
            }
        }

        this._authenticate(function(data) {
            if (data.status === 'ok') {
                // Find all internal links and append the token and username to it!
                var newSearchVal = '';
                $('a').each(function () {
                    if (this.href.indexOf(window.location.origin) !== -1) {
                        newQueryString = ftppApi._setQueryVariable('token', ftppApi._getQueryVariable('token'), this.href);
                        newQueryString = ftppApi._setQueryVariable('username', ftppApi.username, this.origin+this.pathname+newQueryString);

                        this.href = this.origin+this.pathname+newQueryString;
                    }
                });

                if (window.location.pathname === '/' && ftppApi.domElements.propertySearchLocation !== false) {
                    ftppApi.domElements.propertySearchLocation.parents('form').append(
                        '<input type="hidden" name="token" value="'+ftppApi._getQueryVariable('token')+'" />'+
                        '<input type="hidden" name="username" value="'+ftppApi.username+'" />'
                    );
                }
            }

            if (window.location.pathname === '/my-bookings.html') {
                if (data.status === 'fail') {
                    // Login failed! redirect to landing page!
                    window.location.replace(window.location.origin);
                } else {
                    // Load the bookings of the user
                    ftppApi.loadUserBookings(
                        ftppApi._getQueryVariable('token'),
                        ftppApi.domElements.userBookingsResultContainer
                    );
                }
            }
        });
    },
    addDomElement: function(role, elementSelector) {
        if (typeof this.domElements[role] !== 'boolean' && typeof this.domElements[role] !== 'object') {
            throw Error('The role '+role+' is not available!');
        }

        this.domElements[role] = jQuery(elementSelector);

        return this;
    },
    setUpEventListeners: function(event, eventAction, elementSelector) {
        // The passed eventAction will be executed on the event!
        if (typeof this.events[eventAction] !== 'function') {
            throw Error('The supplied eventAction '+eventAction+' is not defined in the ftppApi!');
        }

        $('body').on(event, elementSelector, this.events[eventAction]);
        return this;
    },
    sendBookingRequest: function(propertyId, propertyName, username, token, callback) {
        this.axios.post('/users/'+username+'/book/'+propertyId, null, {
            headers: {
                'X-Token': token,
            }
        })
            .then(function (response) {
                console.log(response);
                ftppApi.domElements.globalAlertContainer.append(
                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '  <strong>Booking request for "'+propertyName+'" succeeded!</strong> You can view all of your booking requests in "My Bookings".' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>'
                );

                if (typeof callback === 'function') {
                    callback(response.data);
                }

                $('.alert.alert-dismissible').alert();
            })
            .catch(function (error) {
                ftppApi.domElements.globalAlertContainer.append(
                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '  <strong>Booking request failed!</strong> Try to book another property or come back later.' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>'
                );


                callback({status: 'fail'});

                $('.alert.alert-dismissible').alert();
            })
            .finally(function () {
                window.scrollTo({top: 0, behavior: 'smooth'});
            });
    },
    loadPropertyList: function(location, destination) {
        // Display loading indicator
        destination.html('<div class="d-flex justify-content-center"><div class="spinner-border text-secondary" role="status"><span class="sr-only">Loading...</span></div></div>');

        var outputBookingRequestDisabled = function () {
            if (ftppApi.isLoggedIn) {
                return '';
            }

            return ' disabled';
        };

        this.axios({
            method: 'get',
            url: '/properties/list/'+encodeURIComponent(location)
        })
            .then(function (response) {
                console.debug(response);
                console.debug(response.data.result.length);
                var result;

                if (response.data.result.length === 0) {
                    destination.html(
                        '<div class="alert alert-success" role="alert">' +
                        '  <h4 class="alert-heading">No results found!</h4>' +
                        '  <p class="mb-0">The place you requested was found but no accommodation near it could be found! Maybe try the next bigger city.</p>' +
                        '</div>'
                    );
                    return;
                }

                var propertyList = '<div class="row property-list">';
                for (var i = 0; i < response.data.result.length; i++) {
                    result = response.data.result[i];

                    propertyList += '<div class="col-sm-6 mb-2">' +
                        '  <div class="card">' +
                        '    <div class="card-body">' +
                        '      <h5 class="card-title">'+result.name+'</h5>' +
                        '      <p class="card-text">'+result.address+'</p>' +
                        '      <button type="button"'+outputBookingRequestDisabled()+' data-ftpp-place-name="'+result.name+'" data-ftpp-place-id="'+result.placeId+'" class="btn btn-primary booking-request-action"><span class="original-text">Make Booking request!</span></a>' +
                        '    </div>' +
                        '  </div>' +
                        '</div>';
                }
                propertyList += '</div>';

                destination.html(propertyList);
            })
            .catch(function (response) {
                //handle error
                console.debug(response);

                destination.html('<div class="alert alert-danger" role="alert">' +
                    '  <h4 class="alert-heading">No Results found!</h4>' +
                    '  <p>Unfortunatly we couldn\'t find any results for your search request. Maybe try one of theses popular searches:</p>' +
                    '  <ul>' +
                    '    <li><a href="'+window.location.pathname+ftppApi._setQueryVariable('q', 'Munich')+'">Munich</a></li>' +
                    '    <li><a href="'+window.location.pathname+ftppApi._setQueryVariable('q', 'New York')+'">New York</a></li>' +
                    '    <li><a href="'+window.location.pathname+ftppApi._setQueryVariable('q', 'Los Angeles')+'">Los Angeles</a></li>' +
                    '    <li><a href="'+window.location.pathname+ftppApi._setQueryVariable('q', 'Copenhagen')+'">Copenhagen</a></li>' +
                    '  </ul>' +
                    '  <hr>' +
                    '  <p class="mb-0">If you can\'t load any results, <i>even the popular ones</i>, then there is a problem in the underlying API providing the results for different locations to us.</p>' +
                    '</div>');
            });
    },
    loadUserBookings: function(token, destination) {
        this._checkIfDomElementsArePresent([
            'globalAlertContainer'
        ], 'loadUserBookings');

        destination.html('<div class="d-flex justify-content-center"><div class="spinner-border text-secondary" role="status"><span class="sr-only">Loading...</span></div></div>');

        this.axios.get('/users/'+ftppApi.username+'/bookings', {
            headers: {
                'X-Token': token,
            }
        })
            .then(function (response) {
                //handle success
                var responseHtml = '<table class="bookings-table">';
                var element;

                if (response.data.bookings.length === 0) {
                    responseHtml += '<tbody><tr><th class="text-center">No booking requests found.</th></tr>';
                } else {
                    responseHtml += '<thead>' +
                        '  <tr>' +
                        '   <th>ID</th>' +
                        '   <th>Property name</th>' +
                        '   <th>City</th>' +
                        '   <th>Property ID</th>' +
                        '  </tr>' +
                        '</thead>';
                }

                for (var i = 0; i < response.data.bookings.length; i++) {
                    element = response.data.bookings[i];
                    console.log(element);

                    responseHtml += '<tr>' +
                        '  <td>'+element.id+'</td>' +
                        '  <td>'+element.property_name+'</td>' +
                        '  <td>'+element.city+'</td>' +
                        '  <td class="text-muted">'+element.property_id+'</td>' +
                        '</tr>';
                }

                responseHtml += '</tbody></table>';
                destination.html(responseHtml);
            })
            .catch(function (response) {
                ftppApi.domElements.globalAlertContainer.append(
                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '  <strong>Loading your booking requests failed!</strong> Try refreshing the page.' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>'
                );

                $('.alert.alert-dismissible').alert();
            });
    },
    register: function(email, username) {
        this._checkIfDomElementsArePresent([
            'registrationForm',
            'registrationButton'
        ], 'register');

        this.domElements.registrationButton.attr("disabled", true);

        this.axios.post('/users/register', {
            email: email,
            username: username
        })
        .then(function (response) {
            //handle success
            var formAlert = ftppApi.domElements.registrationForm.find('.alert');
            if (formAlert.length > 0) {
                formAlert.alert('close');
                formAlert.alert('dispose');
            }

            console.debug(response);
            ftppApi.domElements.registrationForm
                .prepend('<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '  <h6 class="alert-heading">You are now successfully registered!</h6>' +
                    '  <p class="mb-0">Please check the inbox of the mail you entered for your login information.</p>' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>');

            $('#register_form .alert').alert();
        })
        .catch(function (response) {
            //handle error
            var formAlert = ftppApi.domElements.registrationForm.find('.alert');
            if (formAlert.length > 0) {
                formAlert.alert('close');
                formAlert.alert('dispose');
            }

            ftppApi.domElements.registrationForm
                .prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '  <h6 class="alert-heading">Something went wrong!</h6>' +
                    '  <p class="mb-0">Unfortunately we could not finish your registration. ' +
                    'The username must only contain characters from a-z and numbers from 0-9.</p>' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>');

            ftppApi.domElements.registrationForm.find('.alert').alert();

            ftppApi.domElements.registrationButton.removeAttr("disabled");
            console.debug(response);
        });
    },
    login: function(email) {
        this._checkIfDomElementsArePresent([
            'loginForm',
            'loginButton'
        ], 'login');

        this.domElements.loginButton.attr("disabled", true);

        this.axios.post('/users/register', {
            email: email
        })
            .then(function (response) {
                //handle success
                var formAlert = ftppApi.domElements.loginForm.find('.alert');
                if (formAlert.length > 0) {
                    formAlert.alert('close');
                    formAlert.alert('dispose');
                }

                console.debug(response);
                ftppApi.domElements.loginForm
                    .prepend('<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                        '  <h6 class="alert-heading">You successfully requested a login!</h6>' +
                        '  <p class="mb-0">Please check the inbox of the mail you entered for your login information.</p>' +
                        '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '    <span aria-hidden="true">&times;</span>' +
                        '  </button>' +
                        '</div>');

                ftppApi.domElements.loginForm.find('.alert').alert();
            })
            .catch(function (response) {
                //handle error
                var formAlert = ftppApi.domElements.loginForm.find('.alert');
                if (formAlert.length > 0) {
                    formAlert.alert('close');
                    formAlert.alert('dispose');
                }

                ftppApi.domElements.loginForm
                    .prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                        '  <h6 class="alert-heading">Something went wrong!</h6>' +
                        '  <p class="mb-0">Unfortunately we could not finish your login request. ' +
                        'If you haven\'t chosen a username yet please use the <a href="#" data-toggle="modal" data-target="#register_modal">Registration Form</a>.</p>' +
                        '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '    <span aria-hidden="true">&times;</span>' +
                        '  </button>' +
                        '</div>');

                ftppApi.domElements.loginForm.find('.alert').alert();

                ftppApi.domElements.loginButton.removeAttr("disabled");
                console.debug(response);
            });
    },
    logout: function() {
        // If a new client ID is present the user needs to log in again.
        localStorage.removeItem('client-id');
    },
    _initSearchBar: function() {
        this._checkIfDomElementsArePresent([
            'propertySearchLocation',
            'propertyPersonsSelect'
        ], '_initSearchBar');

        var search = this.domElements.propertySearchLocation;
        var persons = this.domElements.propertyPersonsSelect;

        var searchValue = this._getQueryVariable(search[0].name);
        var personsValue = this._getQueryVariable(persons[0].name);

        if (searchValue !== false && searchValue !== '') {
            // Fix url encoded spaces
            search.val(searchValue.split('+').join(' '));
        }

        if (personsValue !== false) {
            persons.val(personsValue);
        }
    },
    _checkIfDomElementsArePresent: function(roles, method) {
        for (var i = 0; i < roles.length; i++) {
            if (typeof this.domElements[roles[i]] !== 'object') {
                throw Error('The method "'+method+'" was called before the '+roles[i]+' DOM element was set but it was required!');
            }
        }

        return true;
    },
    _authenticate: function(callback) {
        this._checkIfDomElementsArePresent([
            'globalAlertContainer'
        ], '_authenticate');

        var authToken = this._getQueryVariable('token');
        var username = this._getQueryVariable('username');

        if (authToken !== false && username !== false) {
            console.debug(authToken);

            this.axios.post('/users/login', {
                username: username
            }, {
                headers: {
                    'X-Token': authToken,
                }
            })
                .then(function (response) {
                    console.log(response);
                    if (response.status === 200) {
                        ftppApi.isLoggedIn = true;
                        ftppApi.username = username;

                        // Improve the following lines...
                        $('.navbar .container-fluid > button.login-modal-opener').replaceWith(
                            '<span class="navbar-text d-none d-lg-inline-block">You are signed in as <i>'+username+'</i></span>'
                        );
                        $('.navbar .navbar-collapse button.login-modal-opener').replaceWith(
                            '<a class="nav-item nav-link" href="/my-bookings.html">My Bookings</a>'
                        );
                        $('.navbar button.register-modal-opener').remove();

                        if (ftppApi.domElements.listResultContainer !== false) {
                            ftppApi.domElements.listResultContainer
                                .find('.booking-request-action').removeAttr('disabled');
                        }
                    }

                    if (typeof callback === 'function') {
                        callback(response.data);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    ftppApi.domElements.globalAlertContainer.append(
                        '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                        '  <strong>Login failed!</strong> Make sure you copy the complete Link from the Mail!' +
                        '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '    <span aria-hidden="true">&times;</span>' +
                        '  </button>' +
                        '</div>'
                    );

                    $('.alert.alert-dismissible').alert();

                    if (typeof callback === 'function') {
                        callback({
                            status: 'fail'
                        });
                    }
                });
        }
    },
    _checkClientId: function() {
        var clientId = localStorage.getItem('client-id');

        if (clientId === null || clientId === '') {
            clientId = this._createClientId();
            localStorage.setItem('client-id', clientId);
        }
    },
    _createClientId: function() {
        var d = new Date().getTime();
        if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
            d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    },
    _getQueryVariable: function(variable, url) {
        var query;

        if (typeof url === 'undefined') {
            query = window.location.search.substring(1);
        } else {
            var urlObj = new URL(url);
            query = urlObj.search.substring(1);
        }
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) === variable) {
                return decodeURIComponent(pair[1]);
            }
        }

        return false;
    },
    _setQueryVariable: function (variable, value, url) {
        var search;

        if (typeof url === 'undefined') {
            search = window.location.search;
        } else {
            var urlObj = new URL(url);
            search = urlObj.search;
        }
        var search_params = new URLSearchParams(search);

        // new value of the variable is set
        search_params.set(variable, value);

        return '?'+search_params.toString();
    }
};
