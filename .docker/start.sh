#!/bin/sh

# Build all required docker images
docker-compose up --build -d

# API dependencies
docker exec -it php_api bash install-composer.sh
docker exec -it php_api php composer.phar install

# Load database schema
docker exec -it php_api php bin/console doctrine:migrations:migrate --no-interaction