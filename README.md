# FTPP
#### Find the perfect Property

Production Demo: [ftpp.mcnn.de](http://ftpp.mcnn.de)

Production API: [gc-ftpp-api.appspot.com](https://gc-ftpp-api.appspot.com/)

[![pipeline status](https://gitlab.com/oberbauz/find-the-perfect-property/badges/master/pipeline.svg)](https://gitlab.com/oberbauz/find-the-perfect-property/commits/master)

## Start the local development server

Please make sure you have docker installed on your local machine and the localhost isn‘t already used by another application. 

If all perquisites are met you should be able to start all required Services with the following Command:
```
./.docker/start.sh
```

The regular application can be reached at [http://localhost/](http://localhost/).
 
The API is located at [http://localhost:8000/](http://localhost:8000/).


### Demo data

For testing purposes some data fixtures where created which can be applied if needed with the following command:

```
docker exec -it php_api php bin/console doctrine:fixtures:load
```

If you want to modify the demo data that is loaded just head to the [AppFixtures.php](api/src/DataFixtures/AppFixtures.php) 
file and modify the the `load` method, execute the command above afterwards again and your data should be loaded.

### Usage

#### From the CLI

Consume the API on localhost via `curl` or other HTTP clients. **Important are the correct headers.**

##### Register action
_A mail will be sent containing the token used to authenticate the User if necessary. 
Directly after the `curl` command succeeds you can retrieve the token with [the API profiler](http://localhost:8000/_profiler/latest?panel=swiftmailer)._
```
curl 'http://localhost:8000/users/register' \
    --data '{"username":"TESTUSER","email":"TEST@example.com"}' \
    -H "Content-Type: application/json" \
    -H 'Accept: application/json, text/plain, */*' \
    -H 'X-Client-Id: 88ff8fc9-73ff-4b5e-b195-7a8acb139552' \
    -H 'User-Agent: CLI/1.0'
```

##### List all bookings for a property
_If the DataFixtures are loaded this should give you a result right away._
```
curl 'http://localhost:8000/properties/ChIJvc36NY91nkcRWoKIMQgQBIc/bookings' \
    -H 'Accept: application/json, text/plain, */*' \
    -H 'X-Client-Id: 88ff8fc9-73ff-4b5e-b195-7a8acb139552' \
    -H 'User-Agent: CLI/1.0'
```

##### List all bookings for a user
_For this request the user has to be authenticated. Take the token from the mail sent to the user and replace `YOUR_GENERATED_TOKEN`._
```
curl 'http://localhost:8000/users/TESTUSER/bookings' \
    -H 'Accept: application/json, text/plain, */*' \
    -H 'X-Client-Id: 88ff8fc9-73ff-4b5e-b195-7a8acb139552' \
    -H 'X-Token: YOUR_GENERATED_TOKEN' \
    -H 'User-Agent: CLI/1.0'
```

### Problems

##### Error on project creation

```
SQLSTATE[HY000] [2002] Connection refused
```

This error happens if the database takes to long to start up. To resolve this issue just execute:

```
docker exec -it php_api php bin/console doctrine:migrations:migrate
```
Everything should be working now.

##### Mail delivery

_This problem can be resolved by added something like mailhog to the Project._

When trying to log in on the local setup the **Mail delivery** will fail!
You can easily resolve this problem by calling [the API profiler](http://localhost:8000/_profiler/latest?panel=swiftmailer) and copying the link from the Mail.

If you want to check out a working Mail delivery modify the `MAILER_URL` in the [.env file](api/.env). 
I used the Gmail service to deliver Mails thus I didn't want to commit my password.

##### Timeouts

It is possible to encounter application timeouts due to the poor performance of docker on mac.
The frontend will timeout requests after 30 seconds - be aware the local setup isn't optimized for performance  

# Tests

### Execute end to end frontend tests locally

_Install nodejs and npm on your machine and you should be ready to go._ 

Execute the following commands directly in the project root.

```
npm install
# If all docker containers are up and running just execute 
npm test
```

### Execute Tests Suite of the API locally

```
docker exec -it php_api php bin/phpunit --testsuite All
```

##### Execute only Unit-Tests for the API locally

```
docker exec -it php_api php bin/phpunit --testsuite Unit
```
