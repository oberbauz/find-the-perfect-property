module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            all: ['Gruntfile.js', 'html/js/main.js', 'html/js/ftppApi.js']
        },
        htmllint: {
            frontend: {
                options: {
                    force: false,
                    'class-style': 'dash',
                    'indent-style': 'spaces',
                    'indent-width': 2
                },
                src: [
                    'html/*.html'
                ]
            }
        },
        bootlint: {
            options: {
                stoponerror: false,
                relaxerror: {
                    'E006': [ 'html/index.html', 'html/list.html' ]
                }
            },
            files: ['html/*.html']
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: ['html/js/ftppApi.js', 'html/js/main.js'],
                dest: 'public/js/main.min.js'
            }
        },
        'string-replace': {
            inline: {
                files: {
                    'public/js/main.min.js': 'public/js/main.min.js'
                },
                options: {
                    replacements: [
                        {
                            pattern: 'http://localhost:8000',
                            replacement: 'https://gc-ftpp-api.appspot.com'
                        }
                    ]
                }
            }
        },
        concat: {
            dist: {
                src: ['html/js/jquery-3.4.1.slim.min.js', 'html/js/bootstrap.bundle.min.js', 'html/js/axios.min.js'],
                dest: 'public/js/lib.min.js'
            }
        },
        copy: {
            img: {
                files: [
                    {
                        expand: true,
                        cwd: 'html',
                        src: ['img/*'],
                        dest: 'public',
                    }
                ],
            },
            htmlfiles: {
                files: [
                    {
                        expand: true,
                        cwd: 'html',
                        src: ['*.html'],
                        dest: 'public',
                    }
                ],
                options: {
                    process: function (content, srcpath) {
                        if (srcpath.indexOf('html/img/') === -1) {
                            // Replace Stylesheets
                            content = content.replace(
                                /(<!-- Stylesheets -->)([^*]+)(<!-- Stylesheets -->\s)/g,
                                '<link href="css/main.css" rel="stylesheet">'
                            );

                            // Replace JS sources
                            content = content.replace(
                                /(<!-- JavaScript -->)([^*]+)(<!-- JavaScript -->\s)/g,
                                '<script src="js/lib.min.js"></script>'+
                                '<script src="js/main.min.js"></script>'
                            );

                            grunt.log.write(srcpath + "\n\n");
                        }

                        return content;
                    },
                }
            },
        },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'public/css/main.css': ['html/css/bootstrap.min.css', 'html/css/style.css']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.loadNpmTasks('grunt-string-replace');

    grunt.loadNpmTasks('grunt-htmllint');

    grunt.loadNpmTasks('grunt-bootlint');

    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Default task(s).
    grunt.registerTask('default', ['jshint', 'htmllint', 'bootlint']);

    grunt.registerTask('dist', ['uglify', 'string-replace', 'concat', 'copy', 'cssmin']);

};